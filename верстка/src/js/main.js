//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/owl.carousel/dist/owl.carousel.min.js
//= ../../node_modules/jquery-slimscroll/jquery.slimscroll.min.js

// jQuery autoComplete v1.0.7
// https://github.com/Pixabay/jQuery-autoComplete
!function (e) {
    e.fn.autoComplete = function (t) {
        var o = e.extend({}, e.fn.autoComplete.defaults, t);
        return "string" == typeof t ? (this.each(function () {
            var o = e(this);
            "destroy" == t && (e(window).off("resize.autocomplete", o.updateSC), o.off("blur.autocomplete focus.autocomplete keydown.autocomplete keyup.autocomplete"), o.data("autocomplete") ? o.attr("autocomplete", o.data("autocomplete")) : o.removeAttr("autocomplete"), e(o.data("sc")).remove(), o.removeData("sc").removeData("autocomplete"))
        }), this) : this.each(function () {
            function t(e) {
                var t = s.val();
                if (s.cache[t] = e, e.length && t.length >= o.minChars) {
                    for (var a = "", c = 0; c < e.length; c++)a += o.renderItem(e[c], t);
                    s.sc.html(a), s.updateSC(0)
                } else s.sc.hide()
            }

            var s = e(this);
            s.sc = e('<div class="autocomplete-suggestions js-autocomplete-suggestions ' + o.menuClass + '"></div>'), s.data("sc", s.sc).data("autocomplete", s.attr("autocomplete")), s.attr("autocomplete", "off"), s.cache = {}, s.last_val = "", s.updateSC = function (t, o) {
                if (s.sc.css({
                        top: s.offset().top + s.outerHeight(),
                        left: s.offset().left,
                        width: s.outerWidth()
                    }), !t && (s.sc.show(), s.sc.maxHeight || (s.sc.maxHeight = parseInt(s.sc.css("max-height"))), s.sc.suggestionHeight || (s.sc.suggestionHeight = e(".autocomplete-suggestion", s.sc).first().outerHeight()), s.sc.suggestionHeight))if (o) {
                    var a = s.sc.scrollTop(), c = o.offset().top - s.sc.offset().top;
                    c + s.sc.suggestionHeight - s.sc.maxHeight > 0 ? s.sc.scrollTop(c + s.sc.suggestionHeight + a - s.sc.maxHeight) : 0 > c && s.sc.scrollTop(c + a)
                } else s.sc.scrollTop(0)
            }, e(window).on("resize.autocomplete", s.updateSC), s.sc.appendTo("body"), s.sc.on("mouseleave", ".autocomplete-suggestion", function () {
                e(".autocomplete-suggestion.selected").removeClass("selected")
            }), s.sc.on("mouseenter", ".autocomplete-suggestion", function () {
                e(".autocomplete-suggestion.selected").removeClass("selected"), e(this).addClass("selected")
            }), s.sc.on("mousedown click", ".autocomplete-suggestion", function (t) {
                var a = e(this), c = a.data("val");
                return (c || a.hasClass("autocomplete-suggestion")) && (s.val(c), o.onSelect(t, c, a), s.sc.hide()), !1
            }), s.on("blur.autocomplete", function () {
                try {
                    over_sb = e(".autocomplete-suggestions:hover").length
                } catch (t) {
                    over_sb = 0
                }
                over_sb ? s.is(":focus") || setTimeout(function () {
                        s.focus()
                    }, 20) : (s.last_val = s.val(), s.sc.hide(), setTimeout(function () {
                    s.sc.hide()
                }, 350))
            }), o.minChars || s.on("focus.autocomplete", function () {
                s.last_val = "\n", s.trigger("keyup.autocomplete")
            }), s.on("keydown.autocomplete", function (t) {
                if ((40 == t.which || 38 == t.which) && s.sc.html()) {
                    var a, c = e(".autocomplete-suggestion.selected", s.sc);
                    return c.length ? (a = 40 == t.which ? c.next(".autocomplete-suggestion") : c.prev(".autocomplete-suggestion"), a.length ? (c.removeClass("selected"), s.val(a.addClass("selected").data("val"))) : (c.removeClass("selected"), s.val(s.last_val), a = 0)) : (a = 40 == t.which ? e(".autocomplete-suggestion", s.sc).first() : e(".autocomplete-suggestion", s.sc).last(), s.val(a.addClass("selected").data("val"))), s.updateSC(0, a), !1
                }
                if (27 == t.which) s.val(s.last_val).sc.hide(); else if (13 == t.which || 9 == t.which) {
                    var c = e(".autocomplete-suggestion.selected", s.sc);
                    c.length && s.sc.is(":visible") && (o.onSelect(t, c.data("val"), c), setTimeout(function () {
                        s.sc.hide()
                    }, 20))
                }
            }), s.on("keyup.autocomplete", function (a) {
                if (!~e.inArray(a.which, [13, 27, 35, 36, 37, 38, 39, 40])) {
                    var c = s.val();
                    if (c.length >= o.minChars) {
                        if (c != s.last_val) {
                            if (s.last_val = c, clearTimeout(s.timer), o.cache) {
                                if (c in s.cache)return void t(s.cache[c]);
                                for (var l = 1; l < c.length - o.minChars; l++) {
                                    var i = c.slice(0, c.length - l);
                                    if (i in s.cache && !s.cache[i].length)return void t([])
                                }
                            }
                            s.timer = setTimeout(function () {
                                o.source(c, t)
                            }, o.delay)
                        }
                    } else s.last_val = c, s.sc.hide()
                }
            })
        })
    }, e.fn.autoComplete.defaults = {
        source: 0,
        minChars: 3,
        delay: 150,
        cache: 1,
        menuClass: "",
        renderItem: function (e, t) {
            t = t.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
            var o = new RegExp("(" + t.split(" ").join("|") + ")", "gi");
            return '<div class="autocomplete-suggestion" data-val="' + e + '">' + e.replace(o, "<b>$1</b>") + "</div>"
        },
        onSelect: function (e, t, o) {
        }
    }
}(jQuery);

$(document).ready(function () {
    var $back_blur = $('.js-dark-background'),
        $mob_menu = $('.js-mob-menu'),
        $form_search = $('.js-form-search'),
        $filters_open = $('.js-filters-open'),
        $filters_close = $('.js-filters-close'),
        $filters_content = $('.filters__content'),
        $search_close = $('.js-search-close'),
        $lang_currency = $('.js-lang-currency'),
        $body = $('body');

    $back_blur.on('click', function () {
        $mob_menu.removeClass('mob-menu_active');
        $filters_content.removeClass('filters__content_active');
        $back_blur.removeClass('dark-background_active');
        $body.removeClass('hidden');
        $lang_currency.fadeOut(300);
        $('.js-popup-ok, .js-popup-error').fadeOut(300);
    });

    $('.js-hamburger-open').on('click', function () {
        $mob_menu.addClass('mob-menu_active');
        $back_blur.addClass('dark-background_active');
        $body.addClass('hidden');
    });

    $('.js-hamburger-close').on('click', function () {
        $mob_menu.removeClass('mob-menu_active');
        $back_blur.removeClass('dark-background_active');
        $body.removeClass('hidden');
    });

    $('.js-tabs-item').on('click', function () {
        var $tabs = $('.js-tabs-item');
        var $menu_content = $('.js-menu-content');
        if (!$(this).hasClass('mob-menu__tabs-item_active')) {
            $tabs.toggleClass('mob-menu__tabs-item_active');
            $menu_content.toggleClass('mob-menu-content_active');
        }
    });

    $('.js-search-button').on('click', function () {
        $form_search.addClass('form-search_mobile');
        $search_close.addClass('form-search__exit_active');
    });

    $search_close.on('click', function () {
        $form_search.removeClass('form-search_mobile');
        $(this).removeClass('form-search__exit_active');
    });

    $('.js-lang-trigger').on('click', function () {
        // $('.header-top-lang__list').toggleClass('header-top-lang__list_active');
        $lang_currency.fadeIn(300);
        $back_blur.addClass('dark-background_active');
        $body.addClass('hidden');
    });

    $('.js-lang-currency-close').on('click', function () {
        $lang_currency.fadeOut(300);
        $back_blur.removeClass('dark-background_active');
        $body.removeClass('hidden');
    });

    $filters_open.on('click', function () {
        $filters_content.addClass('filters__content_active');
        $back_blur.addClass('dark-background_active');
        $body.addClass('hidden');
    });
    $filters_close.on('click', function () {
        $filters_content.removeClass('filters__content_active');
        $back_blur.removeClass('dark-background_active');
        $body.removeClass('hidden');
    });
});


$(function () {
    var $image = $('.js-banner-img');

    if ($(window).width() > 767) {
        $image.each(function () {
            var $img_pc = $(this).attr('data-img-pc');
            $(this).attr('src', $img_pc);
        });
    }

    else {
        $image.each(function () {
            var $img_xs = $(this).attr('data-img-xs');
            $(this).attr('src', $img_xs);
        });
    }
});

$(function () {
    $('.js-public-slider').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        dots: false,
        navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"]
    });
});

$(function () {
    $('.js-product-gallery-carousel').owlCarousel({
        items: 1,
        loop: true,
        dots: false,
        nav: false
    });
});

$(function () {
    $('.product-gallery').each(function () {
        var $this = $(this);
        var owl = $this.find('.js-product-gallery-carousel');
        var preview = $this.find('.js-product-gallery-preview');
        preview.each(function (index) {
            $(this).click(function () {
                owl.trigger('to.owl.carousel', index);
            });
        });
    });
});

$(function () {
    $('.js-filters-box__block').click(function () {
        var $this = $(this);
        var $icon = $this.find('.filters-box__block-title-icon');
        var $content = $this.next('.js-filters-box__list');
        if ($content.hasClass('hidden')) {
            $content.removeClass('hidden');
            $content.slideDown(300);
            $icon.css('transform', 'rotate(0deg)');
        }
        else {
            $content.addClass('hidden');
            $content.slideUp(300);
            $icon.css('transform', 'rotate(180deg)');
        }
    });
});

function sticky_content(block, content) {
    var a = document.querySelector(block), b = null, P = 0;
    window.addEventListener('scroll', Ascroll, false);
    document.body.addEventListener('scroll', Ascroll, false);

    function Ascroll() {
        if (b == null) {
            var Sa = getComputedStyle(a, ''), s = '';
            for (var i = 0; i < Sa.length; i++) {
                if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                    s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                }
            }
            b = document.createElement('div');
            b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
            a.insertBefore(b, a.firstChild);
            var l = a.childNodes.length;
            for (var i = 1; i < l; i++) {
                b.appendChild(a.childNodes[1]);
            }
            a.style.height = b.getBoundingClientRect().height + 'px';
            a.style.padding = '0';
            a.style.border = '0';
        }
        var Ra = a.getBoundingClientRect(),
            R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector(content).getBoundingClientRect().bottom);  // селектор блока, при достижении нижнего края которого нужно открепить прилипающий элемент
        if ((Ra.top - P) <= 0) {
            if ((Ra.top - P) <= R) {
                b.className = 'relative';
                b.style.top = -R + 'px';
            } else {
                b.className = 'fixed';
                b.style.top = P + 'px';
            }
        } else {
            b.className = '';
            b.style.top = '';
        }
        window.addEventListener('resize', function () {
            a.children[0].style.width = getComputedStyle(a, '').width
        }, false);
    }
}

$(function () {
    if ($(window).width() > 992 && $('.js-sticky-content').length > 0) {
        sticky_content('.js-sticky-content', '.js-sticky-box');
    }
});

$(function () {
    $('.form-contact').submit(function (e) {
        e.preventDefault();

        var $form = $(this),
            $back_blur = $('.js-dark-background'),
            $send_button = $('.js-form-button');

        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function () {
                $back_blur.addClass('dark-background_active');
                $('body').addClass('hidden');
                $('.js-popup-ok').fadeIn(300);
                $send_button.removeClass('form-contact__loader');
                $form.trigger('reset');
            },
            error: function () {
                $back_blur.addClass('dark-background_active');
                $('body').removeClass('hidden');
                $('.js-popup-error').fadeIn(300);
                $form.trigger('reset');
                $send_button.removeClass('form-contact__loader');
            },
            beforeSend: function () {
                $send_button.addClass('form-contact__loader');
            }
        });
    });
});

$(function () {
    $('.js-popup-close').on('click', function () {
        $(this).parent().fadeOut(500);
        $('.js-dark-background').removeClass('dark-background_active');
        $('body').removeClass('hidden');
    });
});

$(function () {
    if (($(window).width() < 992)) {
        $('.filters-box').slimScroll({
            size: '4px',
            height: '100%',
            alwaysVisible: true,
            distance: '0',
            railVisible: false,
            background: '#000'
        });
    }
});

$(function () {
    $('.js-product-select').change(function () {
        window.location.href = $('.js-product-select option:selected').val();
    })
});

$(function () {
    var $to_up = $('.js-to-up');
    $to_up.fadeOut();
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $to_up.fadeIn();
        } else {
            $to_up.fadeOut();
        }
    });
    $to_up.click(function () {
        $('body,html').animate({scrollTop: 0}, 800);
    });
});

$(document).ready(function () {
    setTimeout(function () {
        $('.js-loader').fadeOut(500);
    }, 600);
});

$(function () {
    $('.js-autocomlete-search').autoComplete({
        minChars: 3,
        delay: 150,
        source: function (term, suggest) {
            $.ajax({
                type: "post",
                data: {id: term},
                url: $('.js-autocomlete-search').attr('data-action_ajax'),
                // url : 'test.json',
                error: function () {
                    console.log('error, didn"t get data');
                },
                success: function (data) {
                    var oJS = data;	//відповідний JSоб'єкт до JSON об'єкту AJAX відповіді
                    var matches = [];
                    for (var i = 0; i < oJS.data.length; ++i) {	// наповнимо масив елементів
                        matches.push(oJS.data[i]);
                    }
                    suggest(matches);
                }
            });
        },
        onSelect: function (event, term, item) {
            $(location).attr('href', item.attr('data-link'));
        },
        renderItem: function (item, search) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-link = "' + item.id + '" data-val="' + item.name + '">'
                + item.name.replace(re, "<b>$1</b>") + '</div>';
        }
    });
});


$(function () {
        $('.js-autocomplete-suggestion').slimScroll({
            size: '5px',
            height: '300px',
            alwaysVisible: true,
            distance: '0',
            railVisible: false,
            background: '#000'
        });
});

$(function () {
    // var $currency = $('.js-currency');
    // var $language = $('.js-language');

    $('.js-language').change(function () {
        if ($(this).find('option:selected').val() === '3') {
            $('.js-currency option[value="usd"]').attr('selected', true);
            // $('.js-currency option[value="usd"]').html('$ USD');
            $('.js-currency option[value="uah"]').attr('selected', false);
        }
        else {
            $('.js-currency option[value="usd"]').attr('selected', false);
            $('.js-currency option[value="uah"]').attr('selected', true);
            // $('.js-currency option[value="usd"]').html('₴ грн.UAH');
        }
    })
});

