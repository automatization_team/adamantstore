<?php

class admin_banner extends AdminTable {
	public $TABLE = 'banner';
	public $IMG_SIZE = 480; // макс высота
	public $IMG_VSIZE = 144;
	public $IMG_RESIZE_TYPE = 1;
	public $IMG_BIG_SIZE = 5000;
	public $IMG_BIG_VSIZE = 5000;
	public $IMG_NUM = 1;
	public $ECHO_NAME = 'title';
	public $SORT = 'sort DESC';
	public $RUBS_NO_UNDER = 1;
//    public $FIELD_UNDER         = 'parent_id';
	public $NAME = "баннеры";
	public $NAME2 = "баннер";
	public $MULTI_LANG = 1;//добавляем поле

	function __construct() {
		$this->fld[] = new Field( "title", "Заголовок", 1, array( 'multiLang' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
		$this->fld[] = new Field( "active", "Опубликовать", 6, array( 'showInList' => 1, 'editInList' => 1 ) );
		$this->fld[] = new Field( "url_page", "Относительный адрес страницы, на которой отображается баннер", 1 );
		$this->fld[] = new Field( "publish_top", "Опубликовать вверху страницы", 6, array( 'showInList' => 1, 'editInList' => 1 ) );
		$this->fld[] = new Field( "publish_bot", "Опубликовать внизу страницы", 6, array( 'showInList' => 1, 'editInList' => 1 ) );
		$this->fld[] = new Field( "publish_mobile", "Принадлежит мобильной версии", 6, array( 'showInList' => 1, 'editInList' => 1 ) );
		$this->fld[] = new Field( "url", "Адрес для прехода при нажатии", 1 );
		$this->fld[] = new Field( "alias", "Alias (генерируеться, если не заполнен)", 1 );
		$this->fld[] = new Field( "sort", "SORT", 4 );
	}

	function afterEdit( $row ) {
		$this->afterAdd( $row );
	}

	function afterAdd( $row ) {
		if ( empty( $row['alias'] ) ) {
			$qup = "UPDATE " . $this->TABLE . " SET alias = '" . Translit( $row['title_1'] ) . "' WHERE id = " . $row['id'];
			pdoExec( $qup );
		}
		YandexTranslate( $row, $this->TABLE );
	}
}

class admin_slide extends AdminTable {
	public $TABLE = 'slide';
	public $IMG_SIZE = 480; // макс высота
	public $IMG_VSIZE = 144;
	public $IMG_RESIZE_TYPE = 1;
	public $IMG_BIG_SIZE = 5000;
	public $IMG_BIG_VSIZE = 5000;
	public $IMG_NUM = 1;
	public $ECHO_NAME = 'title';
	public $SORT = 'sort DESC';
	public $RUBS_NO_UNDER = 1;
//    public $FIELD_UNDER         = 'parent_id';
	public $NAME = "слайды";
	public $NAME2 = "слайд";
	public $MULTI_LANG = 1;//добавляем поле

	function __construct() {
		$this->fld[] = new Field( "title", "Заголовок", 1, array( 'multiLang' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
		$this->fld[] = new Field( "active", "Опубликовать", 6, array( 'showInList' => 1, 'editInList' => 1 ) );
		$this->fld[] = new Field( "url_page", "Относительный адрес страницы, на которой отображается баннер", 1 );
		$this->fld[] = new Field( "url", "Адрес для прехода при нажатии", 1 );
		$this->fld[] = new Field( "alias", "Alias (генерируеться, если не заполнен)", 1 );
		$this->fld[] = new Field( "sort", "SORT", 4 );
	}

	function afterEdit( $row ) {
		$this->afterAdd( $row );
	}

	function afterAdd( $row ) {
		if ( empty( $row['alias'] ) ) {
			$qup = "UPDATE " . $this->TABLE . " SET alias = '" . Translit( $row['title_1'] ) . "' WHERE id = " . $row['id'];
			pdoExec( $qup );
		}
		YandexTranslate( $row, $this->TABLE );
	}
}

class admin_callback extends AdminTable {
	public $TABLE = 'callback';

	public $ECHO_NAME = 'f_name';
	public $SORT = 'id DESC';
	public $RUBS_NO_UNDER = 1;
//    public $FIELD_UNDER         = 'parent_id';
	public $NAME = "заказы на обратный звонок";
	public $NAME2 = "заказ на обратный звонок";
//	public $MULTI_LANG = 1;//добавляем поле

	function __construct() {
		$this->fld[] = new Field( "f_name", "Имя", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
		$this->fld[] = new Field( "l_name", "Фамилия", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
		$this->fld[] = new Field( "country", "Страна", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
		$this->fld[] = new Field( "mail", "E-mail", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
		$this->fld[] = new Field( "Message", "Текст сообщения", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
	}

//	function afterEdit( $row ) {
//		$this->afterAdd( $row );
//	}

//	function afterAdd( $row ) {
//		if ( empty( $row['alias'] ) ) {
//			$qup = "UPDATE " . $this->TABLE . " SET alias = '" . Translit( $row['title_1'] ) . "' WHERE id = " . $row['id'];
//			pdoExec( $qup );
//		}
//		//YandexTranslate( $row, $this->TABLE );
//	}
}

//class admin_order extends AdminTable {
//	public $TABLE = 'order';
//
//	public $ECHO_NAME = 'f_name';
//	public $SORT = 'id DESC';
//	public $RUBS_NO_UNDER = 1;
////    public $FIELD_UNDER         = 'parent_id';
//	public $NAME = "заказы на обратный звонок";
//	public $NAME2 = "заказ на обратный звонок";
////	public $MULTI_LANG = 1;//добавляем поле
//
//	function __construct() {
//		$this->fld[] = new Field( "f_name", "Имя", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
//		$this->fld[] = new Field( "l_name", "Фамилия", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
//		$this->fld[] = new Field( "country", "Страна", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
//		$this->fld[] = new Field( "mail", "E-mail", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
//		$this->fld[] = new Field( "Message", "Текст сообщения", 1, array( 'editInList' => 1 ) );//, array('multiLang'=>1) добавляем в переменной мультиязычная ли она
//		$this->fld[] = new Field( "product_id", "ID продукта", 1, array( 'editInList' => 1 ) );
//	}
//
////	function afterEdit( $row ) {
////		$this->afterAdd( $row );
////	}
//
////	function afterAdd( $row ) {
////		if ( empty( $row['alias'] ) ) {
////			$qup = "UPDATE " . $this->TABLE . " SET alias = '" . Translit( $row['title_1'] ) . "' WHERE id = " . $row['id'];
////			pdoExec( $qup );
////		}
////		//YandexTranslate( $row, $this->TABLE );
////	}
//}