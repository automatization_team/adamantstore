<?php

namespace app\components;

use app\components\BaseController;
use SimpleXMLElement;

class ExchangeController extends BaseController {
	static public function getExchangeFactorByCurrency( $currency ) {

		$url  = "https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5";
		$curl = curl_init( $url );
		$str  = '';
		if ( $curl ) {
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
			$page = curl_exec( $curl );
			curl_close( $curl );
			unset( $curl );
			$xml = new SimpleXMLElement( $page );

			if ( $currency == 'rub' ) {
				return (real) $xml->row[2]->exchangerate['sale'][0];
			} elseif ( $currency == 'usd' ) {
				return (real) $xml->row[0]->exchangerate['sale'][0];
			} elseif ( $currency == 'eur' ) {
				return (real) $xml->row[1]->exchangerate['sale'][0];
			} elseif ( $currency == 'uah' ) {
				return (real) '1';
			} else {
				return 0;
			}
		}

		return 0;
	}

	static public function priceChange( $value ) {
		$value = ( empty( $_SESSION['currency'] ) ) ? $value : $value / $_SESSION['currency'];

		return self::priceFormat( $value );
	}

	static public function priceFormat( $value ) {
		$value = number_format( $value, 2, ',', ' ' );
		if ( $_SESSION['currency_post'] !== 'uah' && stristr( $value, ',' ) !== false ) {
			$value[ - 1 ] = '9';
		}
		if ( $_SESSION['currency_post'] === 'rub' ) {
			$value[ - 1 ] = '0';
			$value[ - 2 ] = '0';
			$value[ - 4 ] = ( (int) $value[ - 4 ] ) + 1;
		}

		return $value;
	}

	public function showXML() {
		$url  = "https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5";
		$curl = curl_init( $url );
		$str  = '';
		if ( $curl ) {
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
			$page = curl_exec( $curl );
			curl_close( $curl );
			unset( $curl );
			$xml = new SimpleXMLElement( $page );

			$str .= $xml->row[2]->exchangerate['ccy'][0] . ': ';
			$str .= $xml->row[2]->exchangerate['buy'][0] . ' - ';
			$str .= $xml->row[2]->exchangerate['sale'][0] . '<br>';
			$str .= $xml->row[0]->exchangerate['ccy'][0] . ': ';
			$str .= $xml->row[0]->exchangerate['buy'][0] . ' - ';
			$str .= $xml->row[0]->exchangerate['sale'][0] . '<br>';
			$str .= $xml->row[1]->exchangerate['ccy'][0] . ': ';
			$str .= $xml->row[1]->exchangerate['buy'][0] . ' - ';
			$str .= $xml->row[1]->exchangerate['sale'][0];
		}

		return $str;
	}
}
