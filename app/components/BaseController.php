<?php

namespace app\components;

use app\components\ExchangeController;

//use app\models\User\User;
use Yii;
use app\models\CatalogCategories;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\Lang;
use app\models\Slovar;
use app\models\Redirect;

class BaseController extends \yii\web\Controller {
	public $default_content;

	public function init() {
		parent::init();
		//var_dump( \app\models\SeoNew::find()->limit( 1 )->one()->info->title );
		$lang                          = Lang::getCurrent();
		$this->view->params['lang']    = $lang;
		$this->view->params['lang_sh'] = mb_substr( ( $lang->name ), 0, 3, 'utf-8' );
		$sort                          = $lang->id == 1 ? 'asc' : 'desc';
		$langs                         = Lang::find()->orderBy( 'id ' . $sort )->all();
		$this->view->params['langs']   = $langs;
		$current_url                   = Yii::$app->request->pathinfo;
		//var_dump($lang);die();
//var_dump(substr($_SERVER['REQUEST_URI'], 0,-1)); die();

		if ( substr( $_SERVER['REQUEST_URI'], - 1 ) == '/' && $_SERVER['REQUEST_URI'] != '/' ) {

			return $this->redirect( substr( $_SERVER['REQUEST_URI'], 0, - 1 ), 301 )->send();
		}
		if ( strstr( $_SERVER['REQUEST_URI'], '/en' ) !== false ) {

			return $this->redirect( str_replace( '/en', '/', $_SERVER['REQUEST_URI'] ), 301 )->send();
		}
		if ( strstr( $_SERVER['REQUEST_URI'], '//' ) !== false ) {
			return $this->redirect( str_replace( '//', '/', $_SERVER['REQUEST_URI'] ), 301 )->send();
		}
		/*$news = \app\models\News::find()->all();
		foreach ($news as $new)
		{
			var_dump($new->info->title);
		}
		die();*/
		//var_dump($this->url_origin(true)); die();

		$slovar = Slovar::find()
		                ->leftJoin( '`slovar_info`', '`slovar_info`.`record_id` = `slovar`.`id`' )
		                ->select( [ '`slovar`.`alias`', '`slovar_info`.`value`' ] )
		                ->where( [ '`slovar_info`.`lang`' => Lang::getCurrentId() ] )
		                ->asArray()
		                ->all();

		$slovar             = ArrayHelper::map( $slovar, 'alias', 'value' );
		$this->view->params = array_merge( $this->view->params, $slovar );
		if ( $lang->by_default ) {
			$this->view->params['lang_url']    = '';
			Yii::$app->homeUrl                 = $this->view->params['home_url'] = '/';
			$this->view->params['current_url'] = $current_url ? "/{$current_url}" : '/';
		} else {
			$this->view->params['lang_url']    = "/{$lang->url}/";
			Yii::$app->homeUrl                 = $this->view->params['home_url'] = "/{$lang->url}/";
			$this->view->params['current_url'] = "/{$lang->url}/{$current_url}";
		}


		/* Yii::$app->view->registerMetaTag([
			 'name'    => 'robots',
			 'content' => 'NOINDEX, NOFOLLOW'
		 ]);*/


		if ( isset( $_GET['page'] ) && ! empty( $_GET['page'] ) && (int) $_GET['page'] > 1 ) {
			Yii::$app->view->registerMetaTag( [
				'name'    => 'robots',
				'content' => 'NOINDEX, FOLLOW'
			] );
		}

		$men_alias   = 'muzhchinam';
		$women_alias = 'zhenschinam';

		$this->view->params['menu']                 = CatalogCategories::find()->where( [ 'parent_id' => $this->getCurrentGenderId() ] )->joinWith( 'info' )->all();
		$this->view->params['mobile_menu']['men']   = CatalogCategories::find()->where( [ 'parent_id' => CatalogCategories::find()->where( [ 'name_alt' => $men_alias ] )->joinWith( 'info' )->one()->id ] )->joinWith( 'info' )->all();
		$this->view->params['mobile_menu']['women'] = CatalogCategories::find()->where( [ 'parent_id' => CatalogCategories::find()->where( [ 'name_alt' => $women_alias ] )->joinWith( 'info' )->one()->id ] )->joinWith( 'info' )->all();

		$session = Yii::$app->session;
		if ( $_SESSION['currency'] == null ) {
			$session['currency'] = ExchangeController::getExchangeFactorByCurrency( 'usd' );
		}
		if ( $_SESSION['currency_post'] == null ) {
			$session['currency_post'] = 'usd';
		}
	}

	public function getCurrentGenderId() {
		$men_url   = 'men';
		$women_url = 'women';

		$men_alias   = 'muzhchinam';
		$women_alias = 'zhenschinam';

		if ( $_GET['gender'] == $men_url ) {
			$parent_id = CatalogCategories::find()->where( [ 'name_alt' => $men_alias ] )->joinWith( 'info' )->one()->id;
		} elseif ( $_GET['gender'] == $women_url ) {
			$parent_id = CatalogCategories::find()->where( [ 'name_alt' => $women_alias ] )->joinWith( 'info' )->one()->id;
		} else {
			$parent_id = null;
		}

		return $parent_id;
	}

	/**
	 * @inheritdoc
	 */
	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
//				'view'  => '@app/views/site/404.php',
			],
		];
	}

	public function changeDate( $in ) {
		$arr = explode( '-', $in );

		return $arr[2] . '.' . $arr[1] . '.' . $arr[0];
	}

	public function registerOG( $title, $description, $img ) {
		$this->view->registerMetaTag( [ 'property' => 'og:title', 'content' => "$title" ] );
		$this->view->registerMetaTag( [ 'property' => 'og:type', 'content' => "website" ] );
		$this->view->registerMetaTag( [ 'property' => 'og:description', 'content' => "$description" ] );
		$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$this->view->registerMetaTag( [ 'property' => 'og:url', 'content' => "$url" ] );
		$this->view->registerMetaTag( [ 'property' => 'og:site_name', 'content' => "Adamant Store" ] );
		$img = 'https://' . $_SERVER['HTTP_HOST'] . $img;
		$this->view->registerMetaTag( [ 'property' => 'og:image', 'content' => "$img" ] );
	}

	protected function full_url( $use_forwarded_host = false ) {
		return $this->url_origin( $use_forwarded_host ) . $_SERVER['REQUEST_URI'];
	}

	protected function url_origin( $use_forwarded_host = false ) {
		$s        = $_SERVER;
		$ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
		$sp       = strtolower( $s['SERVER_PROTOCOL'] );
		$protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
		$port     = $s['SERVER_PORT'];
		$port     = ( ( ! $ssl && $port == '80' ) || ( $ssl && $port == '443' ) ) ? '' : ':' . $port;
		$host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
		$host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;

		return $protocol . '://' . $host;
	}

	public static function getUrl() {
		$url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
		$url .= ( $_SERVER["SERVER_PORT"] != 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
		$url .= $_SERVER["REQUEST_URI"];
		return $url;
	}
}
