<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slide_info".
 *
 * @property integer $record_id
 * @property integer $lang
 * @property string $title
 *
 * @property Slide $record
 */
class SlideInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slide_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'title'], 'required'],
            [['record_id', 'lang'], 'integer'],
            [['title'], 'string', 'max' => 250],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Slide::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'title' => 'Заголовок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(Slide::className(), ['id' => 'record_id']);
    }

    /**
     * @inheritdoc
     * @return SlideInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SlideInfoQuery(get_called_class());
    }
}
