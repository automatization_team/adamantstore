<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "callback".
 *
 * @property integer $id
 * @property string $f_name
 * @property string $l_name
 * @property string $country
 * @property string $mail
 * @property string $Message
 */
class Callback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'callback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f_name', 'l_name', 'country', 'mail', 'Message'], 'required'],
            [['f_name', 'l_name', 'country', 'mail', 'Message'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'f_name' => 'Имя',
            'l_name' => 'Фамилия',
            'country' => 'Страна',
            'mail' => 'E-mail',
            'Message' => 'Текст сообщения',
        ];
    }

    /**
     * @inheritdoc
     * @return CallbackQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CallbackQuery(get_called_class());
    }
}
