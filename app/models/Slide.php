<?php

namespace app\models;

use Yii;
use app\components\behavior\ImgBehavior;
/**
 * This is the model class for table "slide".
 *
 * @property integer $id
 * @property integer $active
 * @property string $url_page
 * @property string $url
 * @property string $alias
 * @property integer $sort
 *
 * @property SlideInfo[] $slideInfos
 */
class Slide extends \yii\db\ActiveRecord
{
	const IMG_COUNT = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slide';
    }
	public function behaviors() { //add
		return [
			'thumb' => [
				'class' => ImgBehavior::className(),
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'url_page', 'url', 'alias', 'sort'], 'required'],
            [['active', 'sort'], 'integer'],
            [['url_page', 'url', 'alias'], 'string', 'max' => 250],
        ];
    }
	public function getInfo() //add
	{
		return $this->hasOne(SlideInfo::className(), ['record_id' => 'id'])->where([SlideInfo::tableName().'.lang' => Lang::getCurrentId()]);
	}
	public function img_count_const(){ //add
		return self::IMG_COUNT;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Опубликовать',
            'url_page' => 'Относительный адрес страницы, на которой отображается баннер',
            'url' => 'Адрес для прехода при нажатии',
            'alias' => 'Alias (генерируеться, если не заполнен)',
            'sort' => 'SORT',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlideInfos()
    {
        return $this->hasMany(SlideInfo::className(), ['record_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return SlideQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SlideQuery(get_called_class());
    }
}
