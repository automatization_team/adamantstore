<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SlideInfo]].
 *
 * @see SlideInfo
 */
class SlideInfoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SlideInfo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SlideInfo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
