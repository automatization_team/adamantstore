<?php

namespace app\models;

use Yii;
use app\components\behavior\ImgBehavior;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property integer $active
 * @property string $url_page
 * @property integer $publish_top
 * @property integer $publish_bot
 * @property integer $publish_mobile
 * @property string $url
 * @property string $alias
 * @property integer $sort
 *
 * @property BannerInfo[] $bannerInfos
 */
class Banner extends \yii\db\ActiveRecord
{
	const IMG_COUNT = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }
	public function behaviors() { //add
		return [
			'thumb' => [
				'class' => ImgBehavior::className(),
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'url_page', 'publish_top', 'publish_bot', 'url', 'alias', 'sort'], 'required'],
            [['active', 'publish_top', 'publish_bot', 'publish_mobile', 'sort'], 'integer'],
            [['url_page', 'url', 'alias'], 'string', 'max' => 250],
        ];
    }
	public function getInfo() //add
	{
		return $this->hasOne(BannerInfo::className(), ['record_id' => 'id'])->where([BannerInfo::tableName().'.lang' => Lang::getCurrentId()]);
	}
	public function img_count_const(){ //add
		return self::IMG_COUNT;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Опубликовать',
            'url_page' => 'Относительный адрес страницы, на которой отображается баннер',
            'publish_top' => 'Опубликовать вверху страницы',
            'publish_bot' => 'Опубликовать внизу страницы',
            'publish_mobile' => 'Publish Mobile',
            'url' => 'Адрес для прехода при нажатии',
            'alias' => 'Alias (генерируеться, если не заполнен)',
            'sort' => 'SORT',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerInfos()
    {
        return $this->hasMany(BannerInfo::className(), ['record_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return BannerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BannerQuery(get_called_class());
    }
}
