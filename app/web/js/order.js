jQuery(function ($) {
    $('.button-order-form-js').click(function (e) {
        e.preventDefault();
        var formData = new FormData($('#form-order-js').get(0));
        console.log(formData);
        jQuery.ajax({
            url: '/order/buy',
            contentType: false,
            processData: false,
            type: 'POST',
            data: formData,
            success: function (data) {
                if (data[data.length - 1] === '0') {
                    data = data.slice(0, -1);
                }
                $(location).attr('href', data);
                // console.log(data);
            }
        });
    });
});
