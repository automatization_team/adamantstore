<?php
use app\widgets\SLinkPager;
use app\controllers\ProductController;
use \yii\helpers\Url;
use app\components\ExchangeController;

?>
<section class="search-result">
    <div class="container">
        <div class="search-result__products">
            <h4 class="search-result__description"><?= $this->params['rezultatu-poiska-po-zaprosu'] ?> "<?= $search ?>
                "</h4>
			<?php if ( ! empty( $posts_cat ) ) : ?>
                <div class="search-result__categories">
                    <p class="search-result__title"><?= $this->params['najdennue-kategorii'] ?></p>
                    <div class="row">
						<?php foreach ( $posts_cat as $item ): ?>
                            <div class="col-xs-6 col-sm-4 col-md-4">
                                <div class="product-card">
                                    <a href="<?= \yii\helpers\Url::toRoute( $content['posts_cat_url'][ $item->id ]['url'] ) ?>"
                                       class="product-card__img-wrapper">
                                        <img src="<?= $item->bimg ?>" alt="" class="product-card__img">
                                    </a>
                                    <div class="product-card__description text-center">
                                        <a href="<?= \yii\helpers\Url::toRoute( $content['posts_cat_url'][ $item->id ]['url'] ) ?>"
                                           class="product-card__brand"><?= $item->info->name ?>
                                            (<?= $content['posts_cat_url'][ $item->id ]['title'] ?>)</a>
                                        <a href="<?= \yii\helpers\Url::toRoute( $content['posts_cat_url'][ $item->id ]['url'] ) ?>"
                                           class="product-card__title"><?= $item->info->txt ?></a>
                                    </div>
                                </div>
                            </div>
						<?php endforeach; ?>
                    </div>
                </div>
			<?php endif; ?>
			<?php if ( ! empty( $posts ) ) : ?>
                <div class="search-result__products">
                    <p class="search-result__title"><?= $this->params['najdennue-tovaru'] ?></p>
                    <div class="row nopadding">
						<?php $publish = []; ?>
						<?php foreach ( $posts as $item ): ?>
							<?php if ( empty( $publish[ $item->model_id ] ) ): ?>
								<?php $publish[ $item->model_id ] = true; ?>
                                <div class="col-xs-6 col-sm-4 col-md-3">
                                    <div class="product-card">
                                        <a href="<?= \yii\helpers\Url::toRoute( $content['posts_url'][ $item->id ] ) ?>"
                                           class="product-card__img-wrapper">
                                            <img src="<?= $item->bimg ?>" alt="" class="product-card__img">
                                        </a>
                                        <div class="product-card__description text-center">
                                            <a href="<?= \yii\helpers\Url::toRoute( $content['posts_url'][ $item->id ] ) ?>"
                                               class="product-card__brand"><?= $item->info->name ?></a>
                                            <a href="<?= \yii\helpers\Url::toRoute( $content['posts_url'][ $item->id ] ) ?>"
                                               class="product-card__title"><?= $item->info->txt ?></a>
                                            <div class="product-card__price">
                                            <span class="product-card__price-value product-card__price-value_current"><?= ExchangeController::priceChange( $item->price_current ) ?>
                                                <span
                                                        class="product-card__currency"><?= $this->params[ $_SESSION['currency_post'] ] ?></span></span>
		                                        <?php if ( ! empty( $item->price_old ) ) : ?><span
                                                        class="product-card__price-value product-card__price-value_old"><?= ExchangeController::priceChange( $item->price_old ) ?>
                                                    <span
                                                            class="product-card__currency"><?= $this->params[ $_SESSION['currency_post'] ] ?></span>
                                                    </span> <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							<?php endif; ?>
						<?php endforeach; ?>
                    </div>
                </div>
			<?php endif; ?>
        </div>
    </div>
</section>
