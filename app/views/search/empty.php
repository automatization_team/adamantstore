<?php
use app\widgets\SLinkPager;
use app\controllers\ProductController;
use \yii\helpers\Url;
?>
<section class="search-result">
	<div class="container">
		<h3 class="search-result__not-found">
			<?=$this->params['k-sozhaleniu-po-vashemu-zaprosu-nichego-ne-najdeno'] ?>
		</h3>
	</div>
</section>