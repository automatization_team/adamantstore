<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\CatalogCategories;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register( $this );
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"></script>
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode( \app\components\Seo::get( 'title' ) ) ?></title>
	<meta name="description" content="<?= Html::encode( \app\components\Seo::get( 'description' ) ) ?>">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="transition-loader transition-loader_active js-loader">
	<div class="transition-loader__inner">
		<label><i class="fas fa-circle"></i></label>
		<label><i class="fas fa-circle"></i></label>
		<label><i class="fas fa-circle"></i></label>
		<label><i class="fas fa-circle"></i></label>
		<label><i class="fas fa-circle"></i></label>
		<label><i class="fas fa-circle"></i></label>
	</div>
</div>
<div class="dark-background js-dark-background"></div>
<form class="lang-currency js-lang-currency" method="post" action="/form/currency">
	<input value="<?= $_SERVER['REQUEST_URI'] ?>" name="url" hidden>
	<div class="lang-currency__head">
		<span class="lang-currency__head-txt"><?= $this->params['vubor-yazuka-i-valutu'] ?></span>
		<span class="lang-currency__close js-lang-currency-close"><i class="fas fa-times popup__close-icon"></i></span>
	</div>

	<p class="lang-currency__title"><?= $this->params['vuberite-yazuk'] ?>:</p>
	<select name="language" class="lang-currency__select lang-currency_language js-language">
		<option value="3" <?= ( $this->params['lang']->url == 'en' ) ? 'selected' : '' ?>>English</option>
		<option value="2" <?= ( $this->params['lang']->url == 'uk' ) ? 'selected' : '' ?>>Українська</option>
		<option value="1" <?= ( $this->params['lang']->url == 'ru' ) ? 'selected' : '' ?>>Русский</option>
	</select>

	<p class="lang-currency__title"><?= $this->params['vuberite-valutu'] ?>:</p>
	<select name="currency" class="lang-currency__select lang-currency_currency js-currency">
		<option value="usd" <?= ( $_SESSION['currency_post'] === 'usd' ) ? 'selected' : '' ?>>$ USD</option>
		<option value="uah" <?= ( $_SESSION['currency_post'] === 'uah' ) ? 'selected' : '' ?>>₴ грн. UAH</option>
		<option value="rub" <?= ( $_SESSION['currency_post'] === 'rub' ) ? 'selected' : '' ?>>₽ руб. RUB</option>
	</select>

	<button type="submit" class="button lang-currency__submit"><?= $this->params['primenit-cur-but'] ?></button>
</form>
<div class="mob-menu js-mob-menu">
	<div class="hamburger-button hamburger-button_active js-hamburger-close">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<div class="mob-menu__content">
		<div class="mob-menu__tabs">
			<button
				class="mob-menu__tabs-item mob-menu__tabs-item_active js-tabs-item"><?= $this->params['muzhchinam'] ?></button>
			<button class="mob-menu__tabs-item js-tabs-item"><?= $this->params['zhenschinam'] ?></button>
		</div>
		<ul class="mob-menu-content mob-menu-content_active js-menu-content">
			<?php foreach ( $this->params['mobile_menu']['men'] as $item ) : ?>
				<li class="mob-menu-content__item">
					<a href="<?= Url::toRoute( '/men/' . $item->name_alt ) ?>"
					   class="mob-menu-content__link"><?= $item->info->name ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
		<ul class="mob-menu-content js-menu-content">
			<?php foreach ( $this->params['mobile_menu']['women'] as $item ) : ?>
				<li class="mob-menu-content__item">
					<a href="<?= Url::toRoute( '/women/' . $item->name_alt ) ?>"
					   class="mob-menu-content__link"><?= $item->info->name ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
<header class="header">
	<div class="header-top container">
		<div class="hamburger-button js-hamburger-open">
			<span></span>
			<span></span>
			<span></span>
		</div>
		<a class="header-top__logo" href="<?= Url::home() ?>">
			<img class="header-top__logo-img" src="/images/logo.png" alt="">
		</a>
		<ul class="header-top-genders header-top__genders">
			<li class="header-top-genders__item<?= ( strpos( $_SERVER['REQUEST_URI'], 'men' ) !== false && strpos( $_SERVER['REQUEST_URI'], 'women' ) === false ) ? ' header-top-genders__item_active' : '' ?>">
				<a href="<?= Url::toRoute( '/men' ) ?>"
				   class="header-top-genders__link"><?= $this->params['muzhchinam'] ?></a>
			</li>
			<li class="header-top-genders__item<?= ( strpos( $_SERVER['REQUEST_URI'], 'women' ) !== false ) ? ' header-top-genders__item_active' : '' ?>">
				<a href="<?= Url::toRoute( '/women' ) ?>"
				   class="header-top-genders__link"><?= $this->params['zhenschinam'] ?></a>
			</li>
		</ul>
		<div class="header-top__form">
			<form action="<?= Url::toRoute( '/search/index' ) ?>" method="post" class="form-search js-form-search">
				<div class="form-search__exit js-search-close">
					<span></span>
					<span></span>
				</div>
				<label class="form-search__field">
					<input type="search" class="form-search__input js-autocomlete-search" name="search-input"
					       placeholder="<?= $this->params['poisk-predmetov_-brendov-i-vdohnoveniya'] ?>" data-action_ajax="<?= Url::toRoute( '/search/modal' ) ?>">
				</label>
				<button class="form-search__submit" type="submit">
					<svg viewBox="0 0 18 18"><title><?= $this->params['poisk'] ?></title>
						<path fill="currentColor" fill-rule="nonzero"
						      d="M7.65 15.3a7.65 7.65 0 1 1 5.997-2.9c-.01.012 3.183 3.297 3.183 3.297l-1.22 1.18s-3.144-3.283-3.154-3.275A7.618 7.618 0 0 1 7.65 15.3zm0-2a5.65 5.65 0 1 0 0-11.3 5.65 5.65 0 0 0 0 11.3z"></path>
					</svg>
				</button>
			</form>
		</div>
		<ul class="header-top-info header-top__info">
			<li class="header-top-info__item">
				<label for="search-input" class="header-top-info__link js-search-button">
					<i class="fas fa-search"></i>
				</label>
			</li>
			<li class="header-top-info__item">
				<a href="<?= Url::toRoute( '/delivery' ) ?>" class="header-top-info__link">
					<i class="fas fa-truck"></i>
				</a>
			</li>
			<li class="header-top-info__item">
				<a href="<?= Url::toRoute( '/contacts' ) ?>" class="header-top-info__link">
					<i class="fas fa-user"></i>
				</a>
			</li>
			<li class="header-top-info__item header-top-lang">
				<span
					class="header-top-info__link header-top-lang__current js-lang-trigger"><?= $this->params['lang']->url ?></span>
				<!--                <ul class="header-top-lang__list">-->
				<!--	                --><?php //foreach ( $this->params['langs'] as $l ): ?>
				<!--                        --><?php //if ($this->params['lang'] != $l) : ?>
				<!--                            <li class="header-top-lang__item">-->
				<!--                                <a href="-->
				<? //= $l->langUrl ?><!--" class="header-top-lang__link">--><? //= $l->url ?><!--</a>-->
				<!--                            </li>-->
				<!--                            --><?php //endif; ?>
				<!--	                --><?php //endforeach; ?>
				<!--                </ul>-->
			</li>
		</ul>
	</div>
	<?php if ( strpos( $_SERVER['REQUEST_URI'], 'men' ) !== false || strpos( $_SERVER['REQUEST_URI'], 'women' ) !== false ) : ?>
		<nav class="main-menu">
			<div class="container main-menu__navigation">
				<ul class="main-menu__content">
					<?php foreach ( $this->params['menu'] as $item ) : ?>
						<li class="main-menu__item">
							<a href="<?= $item->url ?>"
							   class="main-menu__link<?= ( strpos( $_SERVER['REQUEST_URI'], $item->name_alt ) !== false) ? ' main-menu__link_active' : '' ?>"><?= $item->info->name ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</nav>
	<?php endif; ?>
</header>
<?= $content ?>
<footer class="footer">
	<div class="container">
		<div class="footer__content">
			<div class="footer__menu">
				<ul class="footer-nav footer__nav">
					<li class="footer-nav__item">
						<a href="<?= Url::toRoute( '/about' ) ?>"
						   class="footer-nav__link"><?= $this->params['o-nas'] ?></a>
					</li>
					<li class="footer-nav__item">
						<a href="<?= Url::toRoute( '/delivery' ) ?>"
						   class="footer-nav__link"><?= $this->params['dostavka'] ?></a>
					</li>
					<li class="footer-nav__item">
						<a href="<?= Url::toRoute( '/terms-of-use' ) ?>"
						   class="footer-nav__link"><?= $this->params['polzovatelskoe-soglashenie'] ?></a>
					</li>
					<li class="footer-nav__item">
						<a href="<?= Url::toRoute( '/contacts' ) ?>"
						   class="footer-nav__link"><?= $this->params['kontaktu'] ?></a>
					</li>
					<li class="footer-nav__item">
						<a href="<?= Url::toRoute( '/return' ) ?>"
						   class="footer-nav__link"><?= $this->params['vozvrat'] ?></a>
					</li>
					<li class="footer-nav__item">
						<a href="<?= Url::toRoute( '/confidentiality' ) ?>"
						   class="footer-nav__link"><?= $this->params['konfidencialnost'] ?></a>
					</li>
					<li class="footer-nav__item">
						<a href="<?= Url::toRoute( '/payment' ) ?>"
						   class="footer-nav__link"><?= $this->params['oplata'] ?></a>
					</li>
				</ul>
				<p class="footer__additional-info"><?= $this->params['2018-audemars-piguet_-le-brassus-politika-konfidencialnosti-usloviya-ekspluatacii'] ?></p>
			</div>
			<div class="footer__buttons-group">
				<div class="footer__contact-us">
					<a href="<?= Url::toRoute( '/contact-us' ) ?>"
					   class="button button_transparent button_md"><?= $this->params['contact-us-button'] ?></a>
				</div>
				<ul class="footer-social footer__social">
					<li class="footer-social__item">
						<a href="" class="footer-social__link">
							<i class="fab fa-vk footer-social__icon"></i>
						</a>
					</li>
					<li class="footer-social__item">
						<a href="" class="footer-social__link">
							<i class="fab fa-instagram footer-social__icon"></i>
						</a>
					</li>
					<li class="footer-social__item">
						<a href="" class="footer-social__link">
							<i class="fab fa-facebook-f footer-social__icon"></i>
						</a>
					</li>
					<li class="footer-social__item">
						<a href="" class="footer-social__link">
							<i class="fab fa-twitter footer-social__icon"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<button class="to-up trigger js-to-up"></button>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
