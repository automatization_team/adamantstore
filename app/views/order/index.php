<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use app\components\BaseController;
use app\components\ExchangeController;

?>
<section class="order-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="order-page__product">
					<div class="order-page__product-box">
						<div class="order-page__product-info">
							<img src="<?= $content['product']->bimg ?>" alt="" class="order-page__product-img">
							<p class="order-page__product-title"><?= $content['product']->info->name ?></p>
						</div>
						<span class="order-page__product-price"><?= ExchangeController::priceChange( $content['product']->price_current ) ?>
                            <span class="order-page__product-currency"><?= $this->params[ $_SESSION['currency_post'] ] ?></span>
                        </span>
					</div>
					<div class="order-page__total-price">
						<span class="order-page__total-label">Сума</span>
						<span class="order-page__total-value"><?= ExchangeController::priceChange( $content['product']->price_current ) ?>
                            <span class="order-page__total-currency"><?= $this->params[ $_SESSION['currency_post'] ] ?></span>
                        </span>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="order-page__form">
					<form id="form-order-js" method="post" class="order-form">
                        <input type="text" name="product_id" value="<?= $content['product']->id ?>" hidden>
						<div class="order-form__block">
							<div class="order-form__block-title">1.Контактна інформація</div>
							<div class="order-form__input-box">
								<input class="order-form__input" type="text" placeholder="Імя" name="name">
								<input class="order-form__input" type="text" placeholder="Прізвище" name="surname">
							</div>
							<input class="order-form__input" type="tel" name="phone" placeholder="Телефон">
							<input class="order-form__input" type="email" name="email" placeholder="email">
						</div>

						<div class="order-form__block">
							<div class="order-form__block-title">2. Спосіб доставки</div>
							<select class="order-form__select" name="country">
								<option>Пожалуйста, выберите страну:</option>
								<option>Англия</option>
								<option>Северная Ирландия</option>
								<option>Шотландия</option>
								<option>Уэльс</option>
							</select>
							<div class="order-form__input-box">
								<input class="order-form__input" type="text" placeholder="Місто" name="city">
								<input class="order-form__input" type="text" placeholder="Область" name="area">
							</div>
							<input class="order-form__input" type="text" name="address" placeholder="Адреса">
						</div>

						<div class="order-form__block">
							<div class="order-form__block-title">3. Спосіб оплати</div>
							<label>Оплата онлайн
								<input type="radio" name="order-type">
							</label>
							<label>Оплата накладеним платежем
								<input type="radio" name="order-type">
							</label>

							<div class="order-form__submit">
								<button class="button button_bordered button_md button-order-form-js">Оформити замовлення</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>