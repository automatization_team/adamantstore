<?php
use yii\helpers\Url;
?>
<section class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumbs__list">
            <li class="breadcrumbs__item">
                <a href="<?= Url::home() ?>" class="breadcrumbs__link">
                    <i class="fas fa-home breadcrumbs__link-icon"></i>
                    <span class="breadcrumbs__link-txt"><?= $this->params['glavnaya'] ?></span>
                </a>
            </li>
            <li class="breadcrumbs__item">
                <a class="breadcrumbs__link"><?= $content['title'] ?></a>
            </li>
        </ol>
    </div>
</section>
<section class="information">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4 js-sticky-box">
                <div class="js-sticky-content">
                    <ul class="information__list">
                        <li class="information__item">
                            <a href="<?= Url::toRoute( '/about' ) ?>" class="information__link"><?=$this->params['o-nas'] ?></a>
                        </li>
                        <li class="information__item">
                            <a class="information__link information__link_active"><?=$this->params['dostavka'] ?></a>
                        </li>
                        <li class="information__item">
                            <a href="<?= Url::toRoute( '/terms-of-use' ) ?>" class="information__link"><?=$this->params['polzovatelskoe-soglashenie'] ?></a>
                        </li>
	                    <li class="information__item">
		                    <a href="<?= Url::toRoute( '/contacts' ) ?>" class="information__link"><?= $this->params['kontaktu'] ?></a>
	                    </li>
                        <li class="information__item">
                            <a href="<?= Url::toRoute( '/return' ) ?>" class="information__link"><?=$this->params['vozvrat'] ?></a>
                        </li>
                        <li class="information__item">
                            <a href="<?= Url::toRoute( '/confidentiality' ) ?>" class="information__link"><?=$this->params['konfidencialnost'] ?></a>
                        </li>
                        <li class="information__item">
                            <a href="<?= Url::toRoute( '/payment' ) ?>" class="information__link"><?=$this->params['oplata'] ?></a>
                        </li>
                    </ul>
                </div>
			</div>
			<div class="col-xs-12 col-md-8">
				<div class="information__content">
					<h4 class="information__title"><?= $content['title'] ?></h4>
                    <p class="information__text"><?= $content['text'] ?>
                    </p>
				</div>
			</div>
		</div>
	</div>
</section>
