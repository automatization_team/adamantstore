<?php
use yii\helpers\Url;
?>
<section class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumbs__list">
			<li class="breadcrumbs__item">
				<a href="<?= Url::home() ?>" class="breadcrumbs__link">
					<i class="fas fa-home breadcrumbs__link-icon"></i>
					<span class="breadcrumbs__link-txt"><?= $this->params['glavnaya'] ?></span>
				</a>
			</li>
			<li class="breadcrumbs__item">
				<a class="breadcrumbs__link"><?= $this->params['contact-us-button'] ?></a>
			</li>
		</ol>
	</div>
</section>
<section class="contacts">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<?= $content['text'] ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<form action="/form/save" class="form-contact" method="post" id="form-contact">
					<label class="form-contact__input-title"><?=$this->params['imya'] ?>*:
						<input type="text" class="form-contact__input" name="first-name" required>
					</label>

					<label class="form-contact__input-title"><?=$this->params['familiya'] ?>*:
						<input type="text" class="form-contact__input" name="last-name" required>
					</label>

					<label class="form-contact__input-title"><?=$this->params['strana'] ?>:</label>
					<select class="form-contact__select-default js-select-change" name="country">
						<option><?=$this->params['pozhalujsta_-vuberite-stranu'] ?>:</option>
						<?php $c_list=explode(';',$this->params['stranu-dlya-formu']); ?>
						<?php foreach ($c_list as $c_list_model) { ?>
                            <option><?=$c_list_model ?></option>
						<?php } ?>
					</select>

					<label class="form-contact__input-title"><?=$this->params['email'] ?>*:
						<input type="email" class="form-contact__input" name="email" required>
					</label>

					<label class="form-contact__input-title"><?=$this->params['soobschenie'] ?>*:
						<textarea class="form-contact__input form-contact__input_message" name="message" form="form-contact"></textarea>
					</label>

					<input type="checkbox" class="form-contact__checkbox" id="checkbox_id" required>
					<label class="form-contact__input-title form-contact__checkbox-custom" for="checkbox_id">
						<?=$this->params['ya-prochital-i-prinyal-politiku-konfidencialnosti-audemars-piguet.'] ?>*
					</label>

					<div class="form-contact__send-block">
						<div class="form-contact__required-txt">* <?=$this->params['polya_-obyazatelnue-dlya-zapolneniya'] ?></div>
						<button class="button button_default button_bordered button_md js-form-button" type="submit"><?=$this->params['otpravit'] ?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<div class="contacts-popup js-popup-ok">
	<div class="popup">
		<button class="popup__close js-popup-close"><i class="fas fa-times popup__close-icon"></i></button>
		<div class="popup__content">
			<?=$this->params['forma-uspeshno-otpravlena'] ?>
		</div>
	</div>
</div>
<div class="contacts-popup js-popup-error">
	<div class="popup">
		<button class="popup__close js-popup-close"><i class="fas fa-times popup__close-icon"></i></button>
		<div class="popup__content">
			<?=$this->params['oshibka_-forma-ne-otpravlena'] ?>
		</div>
	</div>
</div>
