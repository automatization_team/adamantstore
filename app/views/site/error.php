<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

?>
<?php  use yii\helpers\Url; ?>
<section class="page-404">
    <div class="container">
        <h1 class="page-404__title">404</h1>
        <p class="page-404__description"><?=$this->params['k-sozhaleniu_-takoj-stranicu-ne-suschestvuet.-veroyatno-ona-bula-udalena_-libo-e'] ?></p>
        <div class="page-404__link">
            <a href="<?= Url::home() ?>" class="button button_default button_bordered button_sm"><?=$this->params['na-glavnuu-stranicu'] ?></a>
        </div>
    </div>
</section>
