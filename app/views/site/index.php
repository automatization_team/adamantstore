<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

?>
<section class="main-banner">
    <img src="" alt="" class="main-banner__img js-banner-img" data-img-xs="<?= $content['banner']['mobile'] ?>" data-img-pc="<?= $content['banner']['main'] ?>">
    <div class="main-banner__content text-center">
        <div class="main-banner__txt-container">
            <span class="main-banner__title"><?=$this->params['eto-adamant'] ?></span>
        </div>
        <div class="main-banner__container">
            <span class="main-banner__description"><?=$this->params['vasha-moda-i-stil'] ?></span>
        </div>
        <div class="main-banner__buttons">
            <a href="<?= Url::toRoute( '/women' ) ?>" class="button button_default button_sm main-banner__link"><?=$this->params['dlya-zhenschin-index'] ?></a>
            <a href="<?= Url::toRoute( '/men' ) ?>" class="button button_default button_sm main-banner__link"><?=$this->params['dlya-muzhchin-index'] ?></a>
        </div>
    </div>
</section>