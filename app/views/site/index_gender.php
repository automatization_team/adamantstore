<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

?>
<section class="public-banner">
    <a href="<?=$content['banner']['top']['url'] ?>" class="public-banner__link">
        <img src="" alt="" class="public-banner__img js-banner-img" data-img-xs="<?= $content['banner']['top']['mobile'] ?>"
             data-img-pc="<?= $content['banner']['top']['main'] ?>">
    </a>
</section>
<section class="moment-section">
    <div class="container">
        <div class="row flex-center-x">
	        <?php foreach ( $this->params['menu'] as $item ) : ?>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="moment-card text-center">
                        <a href="<?= $item->url ?>" class="moment-card__img-wrapper">
                            <img src="<?= $item->bimg ?>" alt="" class="moment-card__img">
                        </a>
                        <div class="moment-card__content">
                            <h3 class="moment-card__title"><?= $item->info->name ?></h3>
                            <p class="moment-card__description"><?= $item->info->txt ?></p>
                            <a href="<?= $item->url ?>" class="button button_default button_bordered button_md"><?=$this->params['magazin-sejchas-button'] ?></a>
                        </div>
                    </div>
                </div>
	        <?php endforeach; ?>
        </div>
    </div>
</section>
<section class="public-banner public-banner_sm">
    <a href="<?=$content['banner']['bot']['url'] ?>" class="public-banner__link">
        <img src="" alt="" class="public-banner__img js-banner-img" data-img-xs="<?= $content['banner']['bot']['mobile'] ?>"
             data-img-pc="<?= $content['banner']['bot']['main'] ?>">
    </a>
</section>