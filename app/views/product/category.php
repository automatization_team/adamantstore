<?php
use yii\helpers\Url;
use app\widgets\SLinkPager;
?>
<section class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumbs__list">
            <li class="breadcrumbs__item">
                <i class="fas fa-home breadcrumbs__link-icon"></i>
                <a href="<?= Url::home() ?>" class="breadcrumbs__link"><?=$this->params['glavnaya'] ?></a>
            </li>
            <li class="breadcrumbs__item">
                <a href="<?= Url::toRoute($content['bread']['gender']['url']) ?>" class="breadcrumbs__link"><?= $content['bread']['gender']['name'] ?></a>
            </li>
            <li class="breadcrumbs__item">
                <a href="<?= Url::toRoute($content['bread']['subcat']['url']) ?>" class="breadcrumbs__link"><?= $content['bread']['subcat']['name'] ?></a>
            </li>
        </ol>
    </div>
</section>
<section class="public-slider">
	<div class="owl-carousel public-slider__wrapper js-public-slider">
        <?php foreach ($content['sliders'] as $item) : ?>
            <a href="<?= $item->url ?>" class="public-slider__item">
                <img src="<?= $item->bimg ?>" alt="" class="public-slider__img">
            </a>
        <?php endforeach; ?>
	</div>
</section>
<section class="moment-section">
	<div class="container">
		<div class="row flex-center-x">
			<?php foreach ($content['items'] as $item) : ?>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="moment-card text-center">
						<a href="<?= $item->url ?>" class="moment-card__img-wrapper">
							<img src="<?= $item->bimg ?>" alt="" class="moment-card__img">
						</a>
						<div class="moment-card__content">
							<h3 class="moment-card__title"><?= $item->info->name ?></h3>
							<p class="moment-card__description"><?= $item->info->txt ?></p>
							<a href="<?= $item->url ?>" class="button button_default button_bordered button_md"><?=$this->params['magazin-sejchas-button'] ?></a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>

	</div>
</section>
<section class="public-banner">
	<a href="<?=$content['banner']['bot']['url'] ?>" class="public-banner__link">
		<img src="" alt="" class="public-banner__img js-banner-img" data-img-xs="<?=$content['banner']['bot']['mobile'] ?>"
		     data-img-pc="<?=$content['banner']['bot']['main'] ?>">
	</a>
</section>