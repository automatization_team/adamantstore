<?php
use app\widgets\SLinkPager;
use app\controllers\ProductController;
use \yii\helpers\Url;
use app\components\ExchangeController;

?>
<section class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumbs__list">
			<li class="breadcrumbs__item">
				<i class="fas fa-home breadcrumbs__link-icon"></i>
				<a href="<?= Url::home() ?>" class="breadcrumbs__link"><?= $this->params['glavnaya'] ?></a>
			</li>
			<li class="breadcrumbs__item">
				<a href="<?= Url::toRoute( $content['bread']['gender']['url'] ) ?>"
				   class="breadcrumbs__link"><?= $content['bread']['gender']['name'] ?></a>
			</li>
			<li class="breadcrumbs__item">
				<a href="<?= Url::toRoute( $content['bread']['cat']['url'] ) ?>"
				   class="breadcrumbs__link"><?= $content['bread']['cat']['name'] ?></a>
			</li>
			<li class="breadcrumbs__item">
				<a href="<?= Url::toRoute( $content['bread']['subcat']['url'] ) ?>"
				   class="breadcrumbs__link"><?= $content['bread']['subcat']['name'] ?></a>
			</li>
		</ol>
	</div>
</section>
<section class="catalog">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 js-sticky-box">
				<div class="filters js-sticky-content">
					<button
						class="button button_default button_bordered button_md filters__button js-filters-open"><?= $this->params['filtru-title-section'] ?></button>
					<div class="filters__content">
						<div class="hamburger-button hamburger-button_active js-filters-close">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<div class="filters-box">
							<a href="<?= Url::toRoute( $content['clear_filters_url'] ) ?>"
							   class="filters-box__clear-all filters-box__clear-all_visible"><?= $this->params['ochistit-filtru'] ?>
								<i class="fas fa-sync filters-box__clear-all-icon"></i>
							</a>
							<?php foreach ( $content['filter'] as $filter ) : ?>
								<div class="filters-box__block">
									<div class="filters-box__block-title js-filters-box__block"><?= $filter['name'] ?><i
											class="fas fa-angle-down filters-box__block-title-icon"></i></div>
									<ul class="filters-box__list js-filters-box__list">
										<?php foreach ( $filter as $item ) : ?>
											<?php foreach ( $item as $li ) : ?>
												<li class="filters-box__item">
													<?php
													$li_url = ( $li['url'][ strlen( $li['url'] ) - 7 ] == '/' && $li['url'][ strlen( $li['url'] ) - 6 ] == 'f' && $li['url'][ strlen( $li['url'] ) - 1 ] == 'r' ) ? str_replace( '/filter', '', $li['url'] ) : $li['url'];
													?>
													<a href="<?= Url::toRouteFilter( $li_url, $content['gender'] ) ?>"
													   class="filters-box__link<?= ( $li['selected'] ) ? ' filters-box__link_active' : '' ?>">
														<?= $li['value_name'] ?> (<?= $li['count'] ?>)
													</a>
												</li>
											<?php endforeach; ?>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php endforeach; ?>
							<div class="filters-box__block">
								<div
									class="filters-box__block-title js-filters-box__block"><?= $this->params['sortirovka'] ?>
									<i
										class="fas fa-angle-down filters-box__block-title-icon"></i></div>
								<ul class="filters-box__list js-filters-box__list">
									<li class="filters-box__item">
										<?php
										$li_url = $_SERVER['REQUEST_URI'];

										$li_url = ProductController::clearPriceUrl( $li_url, $content );

										$li_url = str_replace( '/sort-price-asc', '/', $li_url );

										$li_url = $li_url . $content['add_to_url_sort_asc'];

										$li_url = ( $li_url[ strlen( $li_url ) - 8 ] == '/' && $li_url[ strlen( $li_url ) - 2 ] == 'r' ) ? str_replace( '/filter/', '', $li_url ) : $li_url;
										?>
										<a href="<?= $li_url ?>"
										   class="filters-box__link<?= ( $content['current_sort_asc'] ) ? ' filters-box__link_active' : '' ?>">
											<?= $this->params['po-vozrastaniu-cenu'] ?>
										</a>
									</li>
									<li class="filters-box__item">
										<?php
										$li_url = $_SERVER['REQUEST_URI'];

										$li_url = ProductController::clearPriceUrl( $li_url, $content );

										$li_url = ( $li_url[ strlen( $li_url ) - 8 ] == '/' && $li_url[ strlen( $li_url ) - 2 ] == 'r' ) ? str_replace( '/filter/', '', $li_url ) : $li_url;
										?>
										<a href="<?= $li_url ?>"
										   class="filters-box__link<?= ( $content['current_sort_desc'] ) ? ' filters-box__link_active' : '' ?>">
											<?= $this->params['po-ubuvaniu-cenu'] ?>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
				<div class="row">
					<?php $publish = []; ?>
					<?php foreach ( $content['products'] as $item ) : ?>
						<?php if ( empty( $publish[ $item->model_id ] ) ): ?>
							<?php $publish[ $item->model_id ] = true; ?>
							<div class="col-xs-6 col-sm-4 col-md-4">
								<div class="product-card">
									<a href="<?= $item->url ?>" class="product-card__img-wrapper">
										<img src="<?= $item->bimg ?>" alt="" class="product-card__img">
									</a>
									<div class="product-card__description text-center">
										<a href="<?= $item->url ?>"
										   class="product-card__brand"><?= $item->info->name ?></a>
										<a href="<?= $item->url ?>"
										   class="product-card__title"><?= $item->info->txt ?></a>
										<div class="product-card__price">
                                            <span class="product-card__price-value product-card__price-value_current"><?= ExchangeController::priceChange( $item->price_current ) ?>
	                                            <span
		                                            class="product-card__currency"><?= $this->params[ $_SESSION['currency_post'] ] ?></span></span>
											<?php if ( ! empty( $item->price_old ) ) : ?><span
												class="product-card__price-value product-card__price-value_old"><?= ExchangeController::priceChange( $item->price_old ) ?>
												<span
													class="product-card__currency"><?= $this->params[ $_SESSION['currency_post'] ] ?></span>
												</span> <?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
				<?php
				echo SLinkPager::widget( [
					'pagination' => $content['pages'],
				] );
				?>
			</div>

		</div>
	</div>
</section>
