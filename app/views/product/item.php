<?php

use yii\helpers\Url;
use app\components\ExchangeController;
use app\components\BaseController;

?>
<section class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumbs__list">
            <li class="breadcrumbs__item">
                <i class="fas fa-home breadcrumbs__link-icon"></i>
                <a href="<?= Url::home() ?>" class="breadcrumbs__link"><?= $this->params['glavnaya'] ?></a>
            </li>
            <li class="breadcrumbs__item">
                <a href="<?= Url::toRoute( $content['bread']['gender']['url'] ) ?>"
                   class="breadcrumbs__link"><?= $content['bread']['gender']['name'] ?></a>
            </li>
            <li class="breadcrumbs__item">
                <a href="<?= Url::toRoute( $content['bread']['cat']['url'] ) ?>"
                   class="breadcrumbs__link"><?= $content['bread']['cat']['name'] ?></a>
            </li>
            <li class="breadcrumbs__item">
                <a href="<?= Url::toRoute( $content['bread']['subcat']['url'] ) ?>"
                   class="breadcrumbs__link"><?= $content['bread']['subcat']['name'] ?></a>
            </li>
            <li class="breadcrumbs__item">
                <a href="<?= Url::toRoute( $content['bread']['item']['url'] ) ?>"
                   class="breadcrumbs__link"><?= $content['bread']['item']['name'] ?></a>
            </li>
        </ol>
    </div>
</section>
<section class="product">
    <div class="container">
        <div class="product__main-info">
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-6">
                    <div class="product__gallery">
                        <div class="product-gallery">
                            <div class="owl-carousel product-gallery-carousel js-product-gallery-carousel">
								<?php foreach ( $content['product']->bimgs as $img ) : ?>
                                    <img src="<?= $img ?>" alt="" class="product-gallery-carousel__img">
								<?php endforeach; ?>
                            </div>
                            <ul class="product-gallery-preview">
								<?php foreach ( $content['product']->bimgs as $img ) : ?>
                                    <li class="product-gallery-preview__item js-product-gallery-preview">
                                        <img src="<?= $img ?>" alt="" class="product-gallery-preview__img">
                                    </li>
								<?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-6">
                    <div class="product__description">
                        <h3 class="product__title">
							<?= $content['product']->info->name ?>
                        </h3>
                        <div class="product__short-description">
							<?= $content['product']->info->txt ?>
                        </div>
                        <div class="product__price">
                            <span
                                    class="product__price-value product__price-value_current"><?= ExchangeController::priceChange( $content['product']->price_current ) ?>
                                <span
                                        class="product__currency"><?= $this->params[ $_SESSION['currency_post'] ] ?></span></span>
							<?php if ( ! empty( $content['product']->price_old ) ) : ?>
                                <span
                                        class="product__price-value product__price-value_old"><?= $content['product']->price_old ?>
                                    <span
                                            class="product__currency"><?= $this->params[ $_SESSION['currency_post'] ] ?></span></span>
							<?php endif; ?>
                        </div>
                        <div class="product__size">
                            <select name="" id="" class="product-select js-product-select">
								<?php foreach ( $content['cur_model_prod'] as $text => $url ) : ?>
                                    <option value="<?= $url ?>"
                                            class="product-select__item" <?= ( $text == $content['current_size'] ) ? 'selected' : '' ?>><?= $text ?></option>
								<?php endforeach; ?>
                            </select>
                        </div>
                        <div class="product__buy">
                            <a href="<?= BaseController::getUrl() . '/order'   ?>"
                               class="button button_buy button_md js-buy-product"><?= $this->params['oformit-zakaz'] ?></a>
                        </div>
                        <div class="product-share product__share">
                            <div class="product-share__title"><?= $this->params['podelitsya'] ?>:</div>
                            <ul class="product-share__buttons">
                                <li class="product-share__item">
                                    <a href="http://vkontakte.ru/share.php?url=<?= urlencode( $content['seo']['url'] ); ?>&title=<?= $content['seo']['title']; ?>&description=<?= $content['seo']['description']; ?>&image=<?= $content['seo']['img']; ?>&noparse=true"
                                       onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false"
                                       target="_parent" class="product-share__link">
                                        <i class="fab fa-vk"></i>
                                    </a>
                                </li>
                                <li class="product-share__item">
                                    <a href="https://www.facebook.com/sharer/sharer.php?kid_directed_site=0&sdk=joey&u=<?= $content['seo']['url'] ?>&display=popup&ref=plugin&src=share_button"
                                       class="product-share__link">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li class="product-share__item">
                                    <a href="https://twitter.com/share?url=<?= urlencode( $content['seo']['url'] ) ?>"
                                       class="product-share__link">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="product__additional-info">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="product-properties">
                        <h4 class="product-properties__title"><?= $content['product']->info->name ?></h4>
                        <table class="product-properties__table">
                            <tbody class="product-properties__table-box">
							<?php foreach ( $content['product']->extraparams as $param ) : ?>
								<?php if ( ! empty( $param['value_name'] ) ) : ?>
                                    <tr class="product-properties__table-row">
                                        <td class="product-properties__table-col"><?= $param['param_name'] ?>:</td>
                                        <td class="product-properties__table-col"><?= $param['value_name'] ?></td>
                                    </tr>
								<?php endif; ?>
							<?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="product-services">
                        <h4 class="product-services__title"><?= $this->params['dostavka-i-vozvrat'] ?></h4>
                        <p class="product-services__description"><?= $this->params['usloviya-dostavki-i-oplatu-tovara'] ?></p>
                        <div class="product-services__link">
                            <a href="<?= Url::toRoute( '/delivery' ) ?>"
                               class="button button_default button_bordered button_md"><?= $this->params['dostavka'] ?></a>
                        </div>
                        <div class="product-services__link">
                            <a href="<?= Url::toRoute( '/return' ) ?>"
                               class="button button_default button_bordered button_md"><?= $this->params['vozvrat'] ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
