<?php

namespace app\controllers;

use app\models\Banner;
use app\models\Slide;
use app\models\CatalogCategories;
use app\models\CatalogProducts;
use app\models\ExtraParamsCache;
use app\models\Lang;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use app\components\BaseController;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\components\Seo;

class ProductController extends BaseController {

	static public function clearPriceUrl( $li_url, $content ) {
		$li_url = str_replace( $content['add_to_url_sort_asc'], '', $_SERVER['REQUEST_URI'] );
		$li_url = str_replace( '-or-' . $content['sort_asc_alias_short'], '', $li_url );
		$li_url = str_replace( $content['sort_asc_alias_short'] . '-or-', '', $li_url );
		$li_url = str_replace( $content['sort_asc_alias'], '', $li_url );
		$li_url = str_replace( '--or-', '', $li_url );
		$li_url = str_replace( '-or--', '', $li_url );
		$li_url = str_replace( '--', '-', $li_url );

		return $li_url;
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => [ 'logout' ],
				'rules' => [
					[
						'actions' => [ 'logout' ],
						'allow'   => true,
						'roles'   => [ '@' ],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => [ 'post' ],
				],
			],
		];
	}

	public function actionIndex( $alias, $product ) {
		$content['category']       = CatalogCategories::find()->byAlias( $alias )->joinWith( 'info' )->limit( 1 )->one();
		$content['product']        = CatalogProducts::find()->byAlias( $product )->joinWith( 'info' )->limit( 1 )->one();
		$cur_model_prod            = CatalogProducts::find()->where( [ 'model_id' => $content['product']->model_id ] )->joinWith( 'info' )->all();
		$content['cur_model_prod'] = [];
		foreach ( $cur_model_prod as $item ) {
			foreach ( $item->extraparams as $param ) {
				if ( $param['param_name_alt'] === 'cvet' ) {
					$param_filter_color = $param['value_name'];
				}
				if ( $param['param_name_alt'] === 'razmer' ) {
					if ( $item->id === $content['product']->id ) {
						$content['current_size'] = $param['value_name'];
					}
					$content['cur_model_prod'][ $param['value_name'] ] = $item->url;
				}
			}
		}

		$men_alias                          = 'muzhchinam';
		$women_alias                        = 'zhenschinam';
		$gender                             = ( $_GET['gender'] == 'men' ) ? $men_alias : $women_alias;
		$model                              = CatalogCategories::find()->where( [ 'name_alt' => $gender ] )->joinWith( 'info' )->one();
		$gender                             = $model;
		$content['bread']['gender']['name'] = $model->info->name;
		$content['bread']['gender']['url']  = '/' . $_GET['gender'];
		$model                              = CatalogCategories::find()->where( [ '`catalog_categories`.`name_alt`' => $_GET['alias'] ] )->joinWith( 'info' )->one();
		$model_cat                          = CatalogCategories::find()->where( [ '`catalog_categories`.`id`' => $model->parent_id ] )->joinWith( 'info' )->one();
		$content['bread']['subcat']['name'] = $model->info->name;
		$content['bread']['subcat']['url']  = '/' . $_GET['gender'] . '/' . $_GET['alias'];
		$content['bread']['cat']['name']    = $model_cat->info->name;
		$content['bread']['cat']['url']     = '/' . $_GET['gender'] . '/' . $model_cat->name_alt;
		$content['bread']['item']['name']   = $content['product']->info->name;
		$content['bread']['item']['url']    = '/' . $_GET['gender'] . '/' . $model_cat->name_alt . '/' . $content['product']->name_alt;

		$current_lang_url = Lang::getCurrent()->url;
		if ( $current_lang_url === 'ru' ) {
			if ( $_GET['gender'] === 'men' ) {
				$param_filter_color = str_replace( 'ый', 'ого', $param_filter_color );
				$param_filter_color = str_replace( 'ий', 'его', $param_filter_color );
			} elseif ( $_GET['gender'] === 'women' ) {
				$param_filter_color = str_replace( 'ий', 'ого', $param_filter_color );
				$param_filter_color = str_replace( 'ый', 'ього', $param_filter_color );
			}
		} elseif ( $current_lang_url === 'uk' ) {
			if ( $_GET['gender'] === 'men' ) {
				$param_filter_color = str_replace( 'ий', 'ого', $param_filter_color );
				$param_filter_color = str_replace( 'ій', 'ього', $param_filter_color );
			} elseif ( $_GET['gender'] === 'women' ) {
				$param_filter_color = str_replace( 'ий', 'ого', $param_filter_color );
				$param_filter_color = str_replace( 'ій', 'ього', $param_filter_color );
			}
		}

		Seo::setByTemplate( $model_cat->name_alt . '-item', [
			'name'    => $content['product']->info->name,
			'articul' => $content['product']->articul,
			'cvet'    => $param_filter_color
		] );

		if ( empty( Seo::get( 'title' ) ) && empty( Seo::get( 'description' ) ) ) {
			Seo::setByTemplate( $_GET['gender'] . '-' . $content['category']->name_alt . '-item', [
				'name'    => $content['product']->info->name,
				'articul' => $content['product']->articul,
				'cvet'    => $param_filter_color
			] );

		}

		$this->registerOG( Seo::get( 'title' ), Seo::get( 'description' ), $content['product']->bimg );

		$content['seo']['title']       = Seo::get( 'title' );
		$content['seo']['description'] = Seo::get( 'description' );
		$content['seo']['img']         = $content['product']->bimg;
		$content['seo']['url']         = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


		return $this->render( 'item', [
			'content' => $content

		] );
	}

	public function actionCatalog( $alias, $filter = "" ) {

		//---сортировка по цене

		$sort_asc_alias       = '-or-sort-price-asc';
		$sort_asc_alias_short = 'sort-price-asc';
		$sort_desc_alias      = ''; //def

		$sort_asc = '`catalog_products`.`price_current` ASC';

		$sort_desc = '`catalog_products`.`price_current` DESC'; //def

		$sort_current = ( strstr( $filter, $sort_asc_alias ) ) ? $sort_asc : $sort_desc;
		$sort_current = ( strstr( $filter, $sort_asc_alias_short ) ) ? $sort_asc : $sort_desc;


		$content['add_to_url_sort_asc']  = ( strlen( $filter ) == 0 ) ? '/filter/' . $sort_asc_alias : $sort_asc_alias;
		$content['sort_asc_alias']       = $sort_asc_alias;
		$content['sort_asc_alias_short'] = $sort_asc_alias_short;

		$filter = ( stristr( $filter, $sort_asc_alias ) == true ) ? str_replace( $sort_asc_alias, '', $filter ) : $filter;
		$filter = ( stristr( $filter, $sort_asc_alias_short ) == true ) ? str_replace( $sort_asc_alias_short, '', $filter ) : $filter;

		$filter = ( $filter[ count( $filter ) - 1 ] == '/' && $filter[ count( $filter ) - 2 ] == 'r' ) ? str_replace( '/filter/', '', $filter ) : $filter;

		$content['current_sort_asc'] = ( $sort_current === $sort_asc ) ? true : false;;
		$content['current_sort_desc'] = ( $sort_current === $sort_desc ) ? true : false;;
		//---конец

		//---подгружаем баннеры и сладеры
		$url_page = str_replace( [ '/ru/', '/en/', '/uk/' ], '/', $_SERVER['REQUEST_URI'] );

		$content['banner']['bot']['main']   = Banner::find()->where( [
			'url_page'       => $url_page,
			'publish_mobile' => 0,
			'active'         => 1,
			'publish_top'    => 0,
			'publish_bot'    => 1
		] )->joinWith( 'info' )->limit( 1 )->one()->bimg;
		$content['banner']['bot']['mobile'] = Banner::find()->where( [
			'url_page'       => $url_page,
			'publish_mobile' => 1,
			'active'         => 1,
			'publish_top'    => 0,
			'publish_bot'    => 1
		] )->joinWith( 'info' )->limit( 1 )->one()->bimg;
		$content['banner']['bot']['url']    = Banner::find()->where( [
			'url_page'       => $url_page,
			'publish_mobile' => 0,
			'active'         => 1,
			'publish_top'    => 0,
			'publish_bot'    => 1
		] )->joinWith( 'info' )->limit( 1 )->one()->url;
		//-
		$content['sliders'] = Slide::find()->where( [
			'url_page' => $url_page,
			'active'   => 1
		] )->joinWith( 'info' )->orderBy( 'sort DESC' )->all();
		//---конец

		$content['gender']            = $_GET['gender'];
		$content['clear_filters_url'] = $content['gender'] . '/' . $alias;

		if ( ! preg_match( "#^[aA-zZ0-9\-_]+$#", $alias ) ) {
			throw new NotFoundHttpException();
		}

		$men_alias             = 'muzhchinam';
		$women_alias           = 'zhenschinam';
		$gender_category_alias = ( $_GET['gender'] == 'men' ) ? $men_alias : $women_alias;
		$gender_category       = CatalogCategories::find()->where( [ 'name_alt' => $gender_category_alias ] )->joinWith( 'info' )->one();

		$content['category'] = CatalogCategories::find()->where( [ 'name_alt' => $_GET['alias'] ] )->andWhere( [ 'parent_id' => $gender_category->id ] )->joinWith( 'info' )->one();

		if ( $content['category']->parent_id === $this->getCurrentGenderId() ) {
			// отображаем странице категории
			$content['items'] = CatalogCategories::find()->where( [ 'parent_id' => $content['category']->id ] )->joinWith( 'info' )->all();

			$men_alias                          = 'muzhchinam';
			$women_alias                        = 'zhenschinam';
			$gender                             = ( $_GET['gender'] == 'men' ) ? $men_alias : $women_alias;
			$model                              = CatalogCategories::find()->where( [ 'name_alt' => $gender ] )->joinWith( 'info' )->one();
			$content['bread']['gender']['name'] = $model->info->name;
			$content['bread']['gender']['url']  = '/' . $_GET['gender'];
			$model                              = CatalogCategories::find()->where( [ '`catalog_categories`.`name_alt`' => $_GET['alias'] ] )->joinWith( 'info' )->one();
			$content['bread']['subcat']['name'] = $model->info->name;
			$content['bread']['subcat']['url']  = '/' . $_GET['gender'] . '/' . $_GET['alias'];

			$cat_seo = '';
			foreach ( $content['items'] as $item ) {
				$cat_seo .= $item->info->name . ', ';
			}
			$cat_seo = substr( $cat_seo, 0, - 2 );
			Seo::setByTemplate( $_GET['gender'] . '-' . $content['category']->name_alt . '-page', [
				'category' => $cat_seo
			] );

			return $this->render( 'category', [ 'content' => $content ] );
		}

		$_categories = CatalogCategories::find()->byAlias( $alias )->joinWith( 'info' )->all();
		foreach ( $_categories as $_category ) :

			$_temp_cat = CatalogCategories::find()->where( ['id' => $_category->parent_id ])->joinWith( 'info' )->all();

//			$_temp_cat_gender = CatalogCategories::find()->where( ['id' => $_temp_cat->parent_id ])->joinWith( 'info' )->one();
			if ($_temp_cat[0]->parent_id === $gender_category->id){
				$_current_category_id = $_category->id;
//				var_dump($_current_category_id);
			}

			endforeach;


		$category = CatalogCategories::find()->where(['id'=>$_current_category_id])->joinWith( 'info' )->one();
//		$category = CatalogCategories::find()->byAlias( $alias )->joinWith( 'info' )->limit( 1 )->one();

		if ( empty( $category ) ) {
			throw new NotFoundHttpException();
		}
		$idsForCatalog = $category->idsForCatalog;
		//	Все значения фильтра в текущей категории
		$filt = ExtraParamsCache::getCategoryFilter( $idsForCatalog, $filter );
//		var_dump($filt);die();

		$selected = ExtraParamsCache::getSelected( $filt );
		$query    = CatalogProducts::find()
		                           ->orderBy( $sort_current )
		                           ->groupBy( '`catalog_products`.`alias`' )
		                           ->groupBy( '`catalog_products`.`model_id`' )
//		                        ->active()
                                   ->byCategoryIds( $idsForCatalog );
//		var_dump($query->all());

		//	Количество всех товаров в категории
		$count_total = $query->count();
		//	Количество выбранных товаров
		$count_current = $count_total;

		if ( $selected ) {
			//	Фильтрация товаров
			$query         = $query->byFilter( $selected );
			$count_current = $query->count();
		}

		//	Постраничная навигация
		$pages = new Pagination( [ 'totalCount' => $count_current, 'pageSize' => 30 ] );

		//	Товары на странице
		$products = $query->offset( $pages->offset )->limit( $pages->limit )->all();

//		if(empty($products)){return $this->render('empty.twig',['category'=>$category]);}

//var_dump($selected);
		if ( ! empty( $selected ) ) {
			$filter_text = '';
			//var_dump($selected['product_id']);die();
			$temp = '';
			foreach ( $selected as $filter_param ) {
				//var_dump($filter_param);
				if ( $temp == $filter_param['param_name'] ) {
					$filter_text .= $filter_param['value_name'] . ', ';
					Yii::$app->view->registerMetaTag( [
						'name'    => 'robots',
						'content' => 'NOINDEX,NOFOLLOW'
					] );
				} else {
					$filter_text .= $filter_param['param_name'] . ':' . $filter_param['value_name'] . ', ';
				}
				$temp = $filter_param['param_name'];
			}
			$filter_text = rtrim( $filter_text, ", " );

//			Seo::setByTemplate( 'default', [
//				'name' => $category->info->name . ' ' . $filter_text,
//
//			] );
			//  var_dump($filter);
			// это тоже часть СЕО-оптимизации
			$filter_array = explode( '-or-', $filter );
			//  var_dump($filter_array);
			/* if(count($filter_array) > 1) {
				 Yii::$app->view->registerMetaTag([
					 'name' => 'robots',
					 'content' => 'NOINDEX,NOFOLLOW'
				 ]);
			 }*/
		} else {
//			Seo::setByTemplate( 'default', [
//				'name' => $category->info->name,
//			] );
		}

//		var_dump($filt); die();
		$content['category'] = $category;
		$content['filter']   = $filt;
		$content['pages']    = $pages;
		$content['products'] = $products;


		$men_alias                          = 'muzhchinam';
		$women_alias                        = 'zhenschinam';
		$gender                             = ( $_GET['gender'] == 'men' ) ? $men_alias : $women_alias;
		$model                              = CatalogCategories::find()->where( [ 'name_alt' => $gender ] )->joinWith( 'info' )->one();
		$content['bread']['gender']['name'] = $model->info->name;
		$content['bread']['gender']['url']  = '/' . $_GET['gender'];
		$model                              = CatalogCategories::find()->where( [ '`catalog_categories`.`name_alt`' => $_GET['alias'] ] )->joinWith( 'info' )->one();
		$model_cat                          = CatalogCategories::find()->where( [ '`catalog_categories`.`id`' => $model->parent_id ] )->joinWith( 'info' )->one();
		$content['bread']['subcat']['name'] = $model->info->name;
		$content['bread']['subcat']['url']  = '/' . $_GET['gender'] . '/' . $_GET['alias'];
		$content['bread']['cat']['name']    = $model_cat->info->name;
		$content['bread']['cat']['url']     = '/' . $_GET['gender'] . '/' . $model_cat->name_alt;


		Seo::setByTemplate( $_GET['gender'] . '-' . $content['category']->name_alt . '-page', [
			'category' => $content['category']->info->name
		] );

		return $this->render( 'catalog', [
			'content' => $content
		] );
	}
}
