<?php

namespace app\controllers;

use app\models\Lang;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use app\components\SeoComponent;
use app\models\CatalogProducts;
use app\models\CatalogCategories;
use yii\data\Pagination;

class SearchController extends \app\components\BaseController {
	public function beforeAction( $action ) {
		$this->enableCsrfValidation = false;

		return parent::beforeAction( $action );
	}

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
//                'only'  => ['index', 'category', 'category-by-type'],
				'rules' => [
					[
						'actions' => [ 'index', 'result' ],
						'allow'   => true,
						'roles'   => [ '?', '@' ],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'index'  => [ 'post', 'get' ],
					'result' => [ 'get', 'post' ],
				],
			],
		];
	}

	public function actionIndex() {
		$search            = Yii::$app->request->post( 'search-input' );
		$session           = Yii::$app->session;
		$session['search'] = $search;

		return $this->redirect( [ '/search/result' ], 301 );

	}

	public function actionResult() {
		$search = Yii::$app->session->get( 'search' );
		$query  = CatalogProducts::find()->joinWith( 'info' )->where( [
			'like',
			'catalog_products_info.name',
			$search
		] );

		/*        SeoComponent::setByTemplate('default', [
					'name' => 'новости',
				]);*/
		$query_count = $query->count();
		$pages       = new Pagination( [ 'totalCount' => $query_count, 'pageSize' => 12 ] );
		$posts       = $query->offset( $pages->offset )->limit( $pages->limit )->orderBy( 'creation_time desc' )->all();

		/*$breadcrumbs = [];
		$breadcrumbs[] = ['label' => "<span itemprop=\"title\">Блог</span>" ];*/
		if(empty($posts)){return $this->render('empty',['search'=>$search]);}

		$content = [];
		$gender_url['zhenschinam']='women';
		$gender_url['muzhchinam']='men';
		foreach ( $posts as $item ) {
			$category                          = CatalogCategories::find()->where( [ 'id' => $item->cat_id ] )->joinWith( 'info' )->one();
			$subcategory                            = CatalogCategories::find()->where( [ 'id' => $category->parent_id ] )->joinWith( 'info' )->one();
			$gender                            = CatalogCategories::find()->where( [ 'id' => $subcategory->parent_id ] )->joinWith( 'info' )->one();
			$content['posts_url'][ $item->id ] = '/'.$gender_url[$gender->name_alt].'/'.$category->name_alt.'/'.$item->alias;
		}

		return $this->render( 'index', [
			'content' => $content,
			'search'  => $search,
			'posts'   => $posts,
			'pages'   => $pages,
		] );
	}


}