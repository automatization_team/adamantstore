<?php

namespace app\controllers;

use app\models\Callback;
use app\models\CatalogCategories;
use app\models\Lang;
use app\models\Slovar;
use Yii;
use yii\filters\AccessControl;
use app\components\BaseController;
use app\components\ExchangeController;
use yii\filters\VerbFilter;

class FormController extends BaseController {

	public function actionSave() {
		$model          = new Callback();
		$model->f_name  = $_POST['first-name'];
		$model->l_name  = $_POST['last-name'];
		$model->country = $_POST['country'];
		$model->mail    = $_POST['email'];
		$model->Message = $_POST['message'];
//		$model->Message='';
		$model->save();

		$message_text = '';
		$message_text .= 'First name: ' . $_POST['first-name'] . '.';
		$message_text .= ' Last name: ' . $_POST['last-name'] . '.';
		$message_text .= ' Country: ' . $_POST['country'] . '.';
		$message_text .= ' Email: ' . $_POST['email'] . '.';
		$message_text .= ' Message: ' . $_POST['message'] . '.';

		$message = Yii::$app->mailer->compose();
		$message->setFrom( 'form@adamantstore.com' );
		$message->setTo( 'request@adamantstore.com' )
		        ->setSubject( 'Message from https://adamantstore.com/contact-us' )
		        ->setTextBody( $message_text );
		$message->send();
	}

	public function actionSaveCurrencyAndLang() {
		$lang     = Lang::find()->andWhere( [ 'id' => $_POST['language'] ] )->limit( 1 )->one();
		$url      = str_replace( [ '/ru', '/uk' ], '/', $_POST['url'] );
		$lang_url = '/' . str_replace( 'en', '', $lang->url );

		$session             = Yii::$app->session;
		$session['currency'] = ExchangeController::getExchangeFactorByCurrency( $_POST['currency'] );;
		$session['currency_post'] = $_POST['currency'];

		$red_url = $_SERVER['SERVER_NAME'] . $lang_url . '/' . $url;
		$red_url = 'https://' . str_replace( '//', '/', $red_url );;

		return $this->redirect( $red_url, 301 )->send();
	}
}
