<?php

namespace app\controllers;

use app\models\Banner;
use app\models\Callback;
use app\components\ExchangeController;
use app\models\CatalogCategories;
use app\models\CatalogProducts;
use app\models\ExtraParamsCache;
use app\models\Lang;
use app\components\BaseController;
use app\components\Seo;
use Ipsp_Api;
use Ipsp_Client;

require_once 'fondy/autoload.php';

class OrderController extends BaseController {

	public function actionIndex( $alias, $product ) {

		$content['category']       = CatalogCategories::find()->byAlias( $alias )->joinWith( 'info' )->limit( 1 )->one();
		$content['product']        = CatalogProducts::find()->byAlias( $product )->joinWith( 'info' )->limit( 1 )->one();
		$cur_model_prod            = CatalogProducts::find()->where( [ 'model_id' => $content['product']->model_id ] )->joinWith( 'info' )->all();
		$content['cur_model_prod'] = [];
		foreach ( $cur_model_prod as $item ) {
			foreach ( $item->extraparams as $param ) {
				if ( $param['param_name_alt'] === 'cvet' ) {
					$param_filter_color = $param['value_name'];
				}
				if ( $param['param_name_alt'] === 'razmer' ) {
					if ( $item->id === $content['product']->id ) {
						$content['current_size'] = $param['value_name'];
					}
					$content['cur_model_prod'][ $param['value_name'] ] = $item->url;
				}
			}
		}

		$men_alias                          = 'muzhchinam';
		$women_alias                        = 'zhenschinam';
		$gender                             = ( $_GET['gender'] == 'men' ) ? $men_alias : $women_alias;
		$model                              = CatalogCategories::find()->where( [ 'name_alt' => $gender ] )->joinWith( 'info' )->one();
		$gender                             = $model;
		$content['bread']['gender']['name'] = $model->info->name;
		$content['bread']['gender']['url']  = '/' . $_GET['gender'];
		$model                              = CatalogCategories::find()->where( [ '`catalog_categories`.`name_alt`' => $_GET['alias'] ] )->joinWith( 'info' )->one();
		$model_cat                          = CatalogCategories::find()->where( [ '`catalog_categories`.`id`' => $model->parent_id ] )->joinWith( 'info' )->one();
		$content['bread']['subcat']['name'] = $model->info->name;
		$content['bread']['subcat']['url']  = '/' . $_GET['gender'] . '/' . $_GET['alias'];
		$content['bread']['cat']['name']    = $model_cat->info->name;
		$content['bread']['cat']['url']     = '/' . $_GET['gender'] . '/' . $model_cat->name_alt;
		$content['bread']['item']['name']   = $content['product']->info->name;
		$content['bread']['item']['url']    = '/' . $_GET['gender'] . '/' . $model_cat->name_alt . '/' . $content['product']->name_alt;

		$current_lang_url = Lang::getCurrent()->url;
		if ( $current_lang_url === 'ru' ) {
			if ( $_GET['gender'] === 'men' ) {
				$param_filter_color = str_replace( 'ый', 'ого', $param_filter_color );
				$param_filter_color = str_replace( 'ий', 'его', $param_filter_color );
			} elseif ( $_GET['gender'] === 'women' ) {
				$param_filter_color = str_replace( 'ий', 'ого', $param_filter_color );
				$param_filter_color = str_replace( 'ый', 'ього', $param_filter_color );
			}
		} elseif ( $current_lang_url === 'uk' ) {
			if ( $_GET['gender'] === 'men' ) {
				$param_filter_color = str_replace( 'ий', 'ого', $param_filter_color );
				$param_filter_color = str_replace( 'ій', 'ього', $param_filter_color );
			} elseif ( $_GET['gender'] === 'women' ) {
				$param_filter_color = str_replace( 'ий', 'ого', $param_filter_color );
				$param_filter_color = str_replace( 'ій', 'ього', $param_filter_color );
			}
		}

		Seo::setByTemplate( $model_cat->name_alt . '-item', [
			'name'    => $content['product']->info->name,
			'articul' => $content['product']->articul,
			'cvet'    => $param_filter_color
		] );

		if ( empty( Seo::get( 'title' ) ) && empty( Seo::get( 'description' ) ) ) {
			Seo::setByTemplate( $_GET['gender'] . '-' . $content['category']->name_alt . '-item', [
				'name'    => $content['product']->info->name,
				'articul' => $content['product']->articul,
				'cvet'    => $param_filter_color
			] );

		}

		$this->registerOG( Seo::get( 'title' ), Seo::get( 'description' ), $content['product']->bimg );

		$content['seo']['title']       = Seo::get( 'title' );
		$content['seo']['description'] = Seo::get( 'description' );
		$content['seo']['img']         = $content['product']->bimg;
		$content['seo']['url']         = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


		return $this->render( 'index', [
			'content' => $content
		] );
	}

	public function actionOrder() {
		$form            = [];
		$form['name']    = $_POST['name'];
		$form['surname'] = $_POST['surname'];
		$form['phone']   = $_POST['phone'];
		$form['email']   = $_POST['email'];
		$form['country'] = $_POST['country'];
		$form['city']    = $_POST['city'];
		$form['area']    = $_POST['area'];
		$form['address'] = $_POST['address'];
		$form['size']    = $_POST['size'];

		$json_form = json_encode( $form, JSON_UNESCAPED_UNICODE );
		$product = CatalogProducts::find()->byId( $_POST['product_id'] )->joinWith( 'info' )->limit( 1 )->one();

		define( 'MERCHANT_ID', 1413008 );
		define( 'MERCHANT_PASSWORD', 'p54ZjEZc2tda3jpeIt56DFfdDbSLQzTR' );
		define( 'IPSP_GATEWAY', 'api.fondy.eu' );
		$client = new Ipsp_Client( MERCHANT_ID, MERCHANT_PASSWORD, IPSP_GATEWAY );
		$ipsp   = new Ipsp_Api( $client );

		$order_id = time();

		if ($_SESSION['currency_post'] == 'usd'){
			$currency = $ipsp::USD;
		}elseif ($_SESSION['currency_post'] == 'uah'){
			$currency = $ipsp::UAH;
		}elseif ($_SESSION['currency_post'] == 'rub'){
			$currency = $ipsp::RUB;
		}

		$amount = str_replace([',','.',' '],'',ExchangeController::priceChange( $product->price_current ));

		$data     = $ipsp->call( 'checkout', array(
			'order_id'      => $order_id,
			'order_desc'    => 'Short Order Description',
			'currency'      => $currency,
			'amount'        => $amount, // 20 USD
			'response_url'  => 'https://adamantstore.com/order/result',
			'product_id'    => $_POST['product_id'],
			'merchant_data' => $json_form
		) )->getResponse();

		return $data->checkout_url;
	}

	public function actionFinal() {
		$form = json_decode( $_POST['merchant_data'] );
//		$this->actionSendLetters($form, $_POST);

		$content = [];
		$product = CatalogProducts::find()->byId( $_POST['product_id'] )->joinWith( 'info' )->limit( 1 )->one();
		$message = "";
		$message .= "<bold>Дані замовлення</bold>". '<br>';
		$message .= "№ замовлення (order_id): " . $_POST['order_id'] . '<br>';
		$message .= "Клієнт (ім’я, прізвище): " . $form->name . $form->surname . '<br>';
		$message .= "Пошта: " . $form->email . '<br>';
		$message .= "Телефон: " . $form->phone . '<br>';
		$message .= "Адрес доставки: " . $form->country . ', ' . $form->city . ', ' . $form->area . ', ' . $form->address . '<br>';
		$message .= "order_status: " . $_POST['order_status'] . '<br>';

		$message .= "<bold>Дані товару</bold>". '<br>';
		$message .= "Назва: " . $product->info->name . '<br>';
		$message .= "Опис: " . $product->info->txt . '<br>';

		$message .= "Ціна: " .
		            str_replace([',','.',' '],'',ExchangeController::priceChange( $product->price_current )) . ' ' .  $_SESSION['currency_post'] . '<br>';


		foreach ( $product->extraparams as $param ) :
			if ( ! empty( $param['value_name'] ) ) :
				$message .= $param['param_name'] . ": " . $param['value_name'] . '<br>';
			endif;
		endforeach;

		return $this->render( 'final', [
			'content' => $message
		] );
	}

	public function actionSaveCurrencyAndLang() {
		$lang     = Lang::find()->andWhere( [ 'id' => $_POST['language'] ] )->limit( 1 )->one();
		$url      = str_replace( [ '/ru', '/uk' ], '/', $_POST['url'] );
		$lang_url = '/' . str_replace( 'en', '', $lang->url );

		$session             = Yii::$app->session;
		$session['currency'] = ExchangeController::getExchangeFactorByCurrency( $_POST['currency'] );;
		$session['currency_post'] = $_POST['currency'];

		$red_url = $_SERVER['SERVER_NAME'] . $lang_url . '/' . $url;
		$red_url = 'https://' . str_replace( '//', '/', $red_url );;

		return $this->redirect( $red_url, 301 )->send();
	}

	private function actionSendLetters( $form, $post ) {
		$product = CatalogProducts::find()->byId( $post['product_id'] )->joinWith( 'info' )->limit( 1 )->one();

		$message = "";
		$message .= "<bold>Дані замовлення</bold>";
		$message .= "№ замовлення (order_id): " . $post['order_id'] . '<br>';
		$message .= "Клієнт (ім’я, прізвище): " . $form->name . $form->surname . '<br>';
		$message .= "Пошта: " . $form->email . '<br>';
		$message .= "Телефон: " . $form->phone . '<br>';
		$message .= "Адрес доставки: " . $form->country . ', ' . $form->city . ', ' . $form->area . ', ' . $form->address . '<br>';
		$message .= "order_status: " . $post['order_status'] . '<br>';

		$message .= "<bold>Дані товару</bold>";
		$message .= "Назва: " . $product->info->name . '<br>';
		$message .= "Опис: " . $product->info->txt . '<br>';

//		$message .= "Ціна: " . ExchangeController::priceChange( $product->price_current ) . ' ' . $this->params[ $_SESSION['currency_post'] ] . '<br>';


		return $this->render( 'final', [
			'value' => $message
		] );
	}
}
