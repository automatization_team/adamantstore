<?php

namespace app\controllers;

use app\models\Banner;
use app\models\CatalogCategories;

use app\models\Pages;
use app\models\Slovar;
use Yii;
use yii\filters\AccessControl;
use app\components\BaseController;
use yii\filters\VerbFilter;

class InfoController extends BaseController {


	/**
	 * Displays contact page.
	 *
	 * @return string
	 */
	public function actionContacts() {
		$model=Pages::find()->where(['alias'=>'contacts'])->joinWith( 'info' )->one();
		$content['text']=$model->info->text;
		$content['title']=$model->info->title;

		return $this->render( 'contacts', [
			'content' => $content,
		] );
	}
	public function actionContactUs() {
		$model=Pages::find()->where(['alias'=>'contact-us'])->joinWith( 'info' )->one();
		$content['text']=$model->info->text;
		$content['title']=$model->info->title;

		return $this->render( 'contact-us', [
			'content' => $content,
		] );
	}

	public function actionAbout() {
		$model=Pages::find()->where(['alias'=>'o-nas'])->joinWith( 'info' )->one();
		$content['text']=$model->info->text;
		$content['title']=$model->info->title;

		return $this->render( 'about', [
			'content' => $content,
		] );
	}
	public function actionDelivery() {
		$model=Pages::find()->where(['alias'=>'dostavka'])->joinWith( 'info' )->one();
		$content['text']=$model->info->text;
		$content['title']=$model->info->title;

		return $this->render( 'delivery', [
			'content' => $content,
		] );
	}
	public function actionTermsOfUse() {
		$model=Pages::find()->where(['alias'=>'polzovatelskoe-soglashenie'])->joinWith( 'info' )->one();
		$content['text']=$model->info->text;
		$content['title']=$model->info->title;

		return $this->render( 'terms-of-use', [
			'content' => $content,
		] );
	}
	public function actionReturn() {
		$model=Pages::find()->where(['alias'=>'vozvrat'])->joinWith( 'info' )->one();
		$content['text']=$model->info->text;
		$content['title']=$model->info->title;

		return $this->render( 'return', [
			'content' => $content,
		] );
	}
	public function actionConfidentiality() {
		$model=Pages::find()->where(['alias'=>'konfidencialnost'])->joinWith( 'info' )->one();
		$content['text']=$model->info->text;
		$content['title']=$model->info->title;

		return $this->render( 'confidentiality', [
			'content' => $content,
		] );
	}
	public function actionPayment() {
		$model=Pages::find()->where(['alias'=>'oplata'])->joinWith( 'info' )->one();
		$content['text']=$model->info->text;
		$content['title']=$model->info->title;

		return $this->render( 'payment', [
			'content' => $content,
		] );
	}
}
