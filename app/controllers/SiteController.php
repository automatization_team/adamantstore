<?php

namespace app\controllers;

use app\models\Banner;
use app\components\Seo;
use app\models\Slovar;
use Yii;
use yii\filters\AccessControl;
use app\components\BaseController;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class SiteController extends BaseController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => [ 'logout' ],
				'rules' => [
					[
						'actions' => [ 'logout' ],
						'allow'   => true,
						'roles'   => [ '@' ],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => [ 'post' ],
				],
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex() {

		$content                      = [];
		$this->view->params['gender'] = null;

		$content['banner']['main']   = Banner::find()->where( [
			'url_page'       => 'home',
			'publish_mobile' => 0,
			'active'         => 1
		] )->joinWith( 'info' )->limit( 1 )->one()->bimg;
		$content['banner']['mobile'] = Banner::find()->where( [
			'url_page'       => 'home',
			'publish_mobile' => 1,
			'active'         => 1
		] )->joinWith( 'info' )->limit( 1 )->one()->bimg;

		Seo::setByTemplate( 'index-page', [
		] );

		return $this->render( 'index', [ 'content' => $content ] );
	}

	public function actionIndexGender() {
		$url_page=str_replace(['/ru/','/en/','/uk/'],'/',$_SERVER['REQUEST_URI']);

		$content['banner']['top']['main']   = Banner::find()->where( [
			'url_page'       => $url_page,
			'publish_mobile' => 0,
			'active'         => 1,
			'publish_top'    => 1,
			'publish_bot'    => 0
		] )->joinWith( 'info' )->limit( 1 )->one()->bimg;
		$content['banner']['top']['mobile'] = Banner::find()->where( [
			'url_page'       => $url_page,
			'publish_mobile' => 1,
			'active'         => 1,
			'publish_top'    => 1,
			'publish_bot'    => 0
		] )->joinWith( 'info' )->limit( 1 )->one()->bimg;

		$content['banner']['bot']['main']   = Banner::find()->where( [
			'url_page'       => $url_page,
			'publish_mobile' => 0,
			'active'         => 1,
			'publish_top'    => 0,
			'publish_bot'    => 1
		] )->joinWith( 'info' )->limit( 1 )->one()->bimg;
		$content['banner']['bot']['mobile'] = Banner::find()->where( [
			'url_page'       => $url_page,
			'publish_mobile' => 1,
			'active'         => 1,
			'publish_top'    => 0,
			'publish_bot'    => 1
		] )->joinWith( 'info' )->limit( 1 )->one()->bimg;

		$content['banner']['top']['url']=Banner::find()->where( [
			'url_page'       => $url_page,
			'publish_mobile' => 0,
			'active'         => 1,
			'publish_top'    => 1,
			'publish_bot'    => 0
		] )->joinWith( 'info' )->limit( 1 )->one()->url;
		$content['banner']['bot']['url']   = Banner::find()->where( [
			'url_page'       => $url_page,
			'publish_mobile' => 0,
			'active'         => 1,
			'publish_top'    => 0,
			'publish_bot'    => 1
		] )->joinWith( 'info' )->limit( 1 )->one()->url;

		Seo::setByTemplate( 'index-gender-'.$_GET['gender'].'-page', [
		] );

		if (empty($content['banner']['top']['main']) && empty($content['banner']['top']['mobile']) && empty($content['banner']['top']['mobile'])) throw new NotFoundHttpException();
		return $this->render( 'index_gender', [ 'content' => $content ] );
	}
	public function actionError() {
		//Seo::setByTemplate('home-page',[]);
		$content                      = [];

		return $this->render( 'error', [ 'content' => $content ] );
	}
}
