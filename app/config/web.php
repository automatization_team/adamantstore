<?php

$params = require( __DIR__ . '/params.php' );
$db     = require( __DIR__ . '/db.php' );

$config = [
	'id'         => 'basic',
	'basePath'   => dirname( __DIR__ ),
	'homeUrl'    => '/',
	'bootstrap'  => [ 'log' ],
	'components' => [
		'language'     => 'en-EN',
		'i18n'         => [
			'translations' => [
				'*' => [
					'class'    => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages',
				],
			],
		],
		// выключаем bootstap
		'assetManager' => [
			'bundles' => [
				'yii\bootstrap\BootstrapAsset'       => [
					'css' => [],
				],
				'yii\bootstrap\BootstrapPluginAsset' => [
					'js' => []
				],
			],
		],
		'request'      => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'enableCsrfValidation' => false,
			'cookieValidationKey'  => 'kossworthburatinocookievalidation',
			'baseUrl'              => '/',    // for multiLang
			'class'                => 'app\components\LangRequest' // for multiLang
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
		'user'         => [
			'identityClass'   => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => [ 'error', 'warning' ],
				],
			],
		],
		'db'           => $db,

		'urlManager' => [
//			'suffix'=>'/',
			'class'               => 'app\components\LangUrlManager', // for multiLang
			'enablePrettyUrl'     => true,
			'showScriptName'      => false,
			'enableStrictParsing' => false,
			'rules'               => [
				'search/modal'              => 'search/modal',
				'search/result/page/<page>' => 'search/result',
				'search/result'             => 'search/result',


				'site/error'    => 'site/error',
				'form/save'     => 'form/save',
				'form/currency' => 'form/save-currency-and-lang',

				'search/index' => 'search/index',

				'order/result' => 'order/final',

				'contacts'        => 'info/contacts',
				'contact-us'      => 'info/contact-us',
				'about'           => 'info/about',
				'delivery'        => 'info/delivery',
				'terms-of-use'    => 'info/terms-of-use',
				'return'          => 'info/return',
				'confidentiality' => 'info/confidentiality',
				'payment'         => 'info/payment',

				'<gender>/<alias>/<product>/order' => 'order/index',
				'order/buy' => 'order/order',


				'<gender>/<alias>/page/<page>/filter/<filter>' => 'product/catalog',
				'<gender>/<alias>/filter/<filter>/page/<page>' => 'product/catalog',
				'<gender>/<alias>/filter/<filter>'             => 'product/catalog',
				'<gender>/<alias>/page/<page>'                 => 'product/catalog',
				'<gender>/<alias>/<product>'                   => 'product/index',
				'<gender>/<alias>'                             => 'product/catalog',

				'<gender>' => 'site/index-gender',
				'/'        => 'site/index',
			],
		],
		'mailer'     => [
			'class' => 'yii\swiftmailer\Mailer',

			'useFileTransport' => false,

			'transport' => [
				'class'    => 'Swift_SmtpTransport',
				'host'     => 'mail.ukraine.com.ua',
				'username' => 'form@adamantstore.com',
				'password' => 'U90l80ejkPPY',
				'port'     => '25',
			],
		],

	],
	'params'     => $params,
];

if ( YII_ENV_DEV ) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][]      = 'debug';
	$config['modules']['debug'] = [
		'class'      => 'yii\debug\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		'allowedIPs' => [ '77.47.231.240', '::1' ],
	];

	$config['bootstrap'][]    = 'gii';
	$config['modules']['gii'] = [
		'class'      => 'yii\gii\Module',
//		 uncomment the following to add your IP if you are not connecting from localhost.
		'allowedIPs' => [ '77.47.231.240', '::1' ],
	];
}

return $config;
